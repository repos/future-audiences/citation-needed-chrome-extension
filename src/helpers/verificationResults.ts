export enum VerificationResult {
  NO_ARTICLES = 'no_articles',
  NO_RESULT = 'no_result',
  QUOTE_FOUND = 'quote_found',
  CORRECT = 'correct',
  INCORRECT = 'incorrect',
  PARTIALLY_CORRECT = 'partially_correct',
  UNKNOWN = 'unknown',
  ERROR = 'error',
}

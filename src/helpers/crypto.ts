export const sha512 = (text: string) => {
  return crypto.subtle
    .digest('SHA-512', new TextEncoder().encode(text))
    .then((buf) => {
      return Array.prototype.map
        .call(new Uint8Array(buf), (x) => ('00' + x.toString(16)).slice(-2))
        .join('');
    });
};

export const todayAsSalt = () => {
  return new Date()
    .toLocaleString('en-GB', { timeZone: 'Europe/London' })
    .split(',')[0];
};

export const hashUserId = (userId: string) => {
  return sha512(userId + todayAsSalt());
};

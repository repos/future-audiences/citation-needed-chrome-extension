import { VerificationResult } from './verificationResults';

export enum ENVIRONMENTS {
  local = 'local',
  dev = 'dev',
  prod = 'prod',
}
export let ENVIRONMENT: ENVIRONMENTS = ENVIRONMENTS.prod;

if (process.env.BUILD_MODE === "DEV") {
  ENVIRONMENT = ENVIRONMENTS.dev;
} else if (process.env.BUILD_MODE === "LOCAL") {
  ENVIRONMENT = ENVIRONMENTS.local;
}

export const BASE_URL =
  (ENVIRONMENT as ENVIRONMENTS) === ENVIRONMENTS.local
    ? 'http://localhost:8000/find_citation'
    : (ENVIRONMENT as ENVIRONMENTS) === ENVIRONMENTS.dev
    ? 'https://citation-needed-api-dev.toolforge.org/find_citation'
    : 'https://citation-needed-api.toolforge.org/find_citation';

export const EXTENDED_VERSION = false;

export const EXTENDED_BASE_URL =
  (ENVIRONMENT as ENVIRONMENTS) === ENVIRONMENTS.local
    ? 'http://localhost:8000/find_citation_customized'
    : (ENVIRONMENT as ENVIRONMENTS) === ENVIRONMENTS.dev
    ? 'https://citation-needed-api-dev.toolforge.org/find_citation_customized'
    : 'https://citation-needed-api.toolforge.org/find_citation_customized';

export const DEFAULT_KEYWORD_INSTRUCTIONS = `Propose the title of a wikipedia article that holds information to support or refute the provided claim. Provide a few keywords and synonyms that can be used to identify sections in arbitrary texts that hold information on the claim. Use the json format {'search_term': 'wikipedia page title', 'keywords': ['keyword1', 'keyword2']}.`;
export const DEFAULT_SECTION_INSTRUCTIONS = `Select the three most important sections from the provided tables of content that would hold the most relevant information to find a citation for the passed statement. Use the following json format: {'Page 1': ['Section 1', 'Section 2'],'Page 2': ['Section 3']}. No explanation or summary.`;
export const DEFAULT_QUOTE_INSTRUCTIONS = `Select a short, VERBATIM, quote from one of the text passages. Choose the section that is most relevant to the claim. Based on the provided sources, make a verdict for whether the claim is 'correct', 'incorrect' or 'partially_correct'. If there is no quote in the sections that is somewhat connected to the statement, return {'no_quote_selected': true}. Otherwise, use the following json format: {'selected_quote': 'Homeopathy is a pseudoscience, a belief that is incorrectly presented ...', result: 'correct', explanation: 'There are matching sources to the claim'}. Use a max length of 300 characters.`;

export const InteractionSteps = [
  'Checking out the statement',
  'Picking out key words',
  'Looking up on Wikipedia',
  'Finding something for you',
];

export const VERIFICATION_RESULTS_TO_ALWAYS_SHOW = [
  VerificationResult.QUOTE_FOUND,
  VerificationResult.NO_RESULT,
  VerificationResult.NO_ARTICLES,
  VerificationResult.ERROR,
];

export const FEEDBACK_FORM_URL =
  'https://docs.google.com/forms/d/e/1FAIpQLSeRjZTFqC6Ao1cFwSDE5eQe-cN2yZhe7fL-nXerRXZ_aE899A/viewform?usp=pp_url';

export const FEEDBACK_FORM_SELECTION_FIELD_ID = 'entry.64753455';
export const FEEDBACK_FORM_QUOTE_FIELD_ID = 'entry.1042256824';
export const FEEDBACK_FORM_EXPLANATION_FIELD_ID = 'entry.793576573';
export const FEEDBACK_FORM_REFERENCE_ID_FIELD_ID = 'entry.1551771272';

export const NUM_VERIFICATIONS_BEFORE_DONATION_BANNER = 5;
export const DONATION_URL =
  'https://donate.wikimedia.org/?utm_medium=CitationNeededExtension&utm_source=April24fixedBanner&utm_campaign=phase1';

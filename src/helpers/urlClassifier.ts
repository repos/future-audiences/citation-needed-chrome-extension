import { NewsSiteList } from './NewsSiteList';

const socialMediaSiteList = [
  ['Youtube', '[^\\?\\/]*?((youtube|googlevideo|ytimg)\\.com|youtu\\.be)'],
  ['Facebook', '[^\\?\\/]*?((facebook\\.(com|co))|(.messenger\\.com))'],
  [
    'Twitter',
    '(t\\.co$)|(t\\.co[^a-zA-Z0-9\\.])|[^\\?\\/]*?((twitter|twimg|twttr)\\.(com|android))',
  ],
  ['Reddit', '[^\\?\\/]*?((reddit\\.com)|(redd\\.it))'],
  ['Tiktok', '([^\\?\\/]*?\\.tiktok\\.com)|(tiktok\\.com)'],
  ['Quora', '[^\\?\\/]*?((quora\\.com)|(quoracdn\\.net))'],
  ['Instagram', '(ig\\.me)|[^\\?\\/]*?((instagram|cdninstagram)\\.com)'],
  ['Ycombinator', '[^\\?\\/]*?((startupschool\\.org)|(ycombinator\\.com))'],
  ['LinkedIn', '[^\\?\\/]*?((linkedin\\.(com|android))|lnkd\\.in)'],
  ['Github', '[^\\?\\/]*?(github\\.com)'],
  [
    'Pinterest',
    '[^\\?\\/]*?((pin\\.it)|(pinimg\\.com)|\\.pinterest)|pinterest',
  ],
  ['Coursera', '[^\\?\\/]*?(coursera\\.(org|com))'],
  ['Medium', '[^\\?\\/]*?(medium\\.com)'],
  [
    'Stackoverflow',
    '[^\\?\\/]*?(((stackexchange|stackapps|askubuntu|blogoverflow|stackoverflow)\\.com)|mathoverflow\\.net)',
  ],
];

export const classifyUrl = (url: string): string => {
  const urlObj = new URL(url);
  const hostname = urlObj.hostname;
  for (const [site, regex] of socialMediaSiteList) {
    if (new RegExp(regex).test(hostname)) {
      return site;
    }
  }

  // strip subdomains
  const strippedHostname = hostname.replace(/(www|m|mobile|blog)\./, '');
  if (NewsSiteList.includes(hostname) || NewsSiteList.includes(strippedHostname)) {
    return 'news-site';
  }
  return 'Others';
};

export const isBannedUrl = (url: string): boolean => {
  // All chrome:// pages are banned
  if (url.includes('chrome://')) {
    return true;
  }

  // for URLs we don't want the extension running on
  const BANNED_DOMAINS = ['chromewebstore.google.com'];

  for (const domain of BANNED_DOMAINS) {
    const urlObj = new URL(url);
    if (urlObj.hostname === domain) {
      return true;
    }
  }
  return false;
};

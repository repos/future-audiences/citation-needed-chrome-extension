// from https://stackoverflow.com/questions/11761563/javascript-regexp-for-splitting-text-into-sentences-and-keeping-the-delimiter
const sentenceRegex =
  /(?=[^])(?:\P{Sentence_Terminal}|\p{Sentence_Terminal}(?!['"`\p{Close_Punctuation}\p{Final_Punctuation}\s]))*(?:\p{Sentence_Terminal}+['"`\p{Close_Punctuation}\p{Final_Punctuation}]*|$)/guy;

export const MAX_SENTENCE_LENGTH = 300;
const MAX_NUMBER_OF_SENTENCES = 3;
export const MAX_SELECTION_LENGTH = 300;

export const splitIntoSentences = (text: string): string[] => {
  return text.match(sentenceRegex) || [];
};

export const splitIntoSelectableSentences = (text: string): string[] => {
  const sentences = splitIntoSentences(text).map((sentence) =>
    sentence.trim().substring(0, MAX_SENTENCE_LENGTH).trim()
  );
  return sentences.slice(0, MAX_NUMBER_OF_SENTENCES);
};

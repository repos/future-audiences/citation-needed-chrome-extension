export const BackendLicences = [
  {
    License: 'Apache 2.0',
    Name: 'asttokens',
    Version: '2.4.1',
  },
  {
    License: 'Apache Software License',
    Name: 'distro',
    Version: '1.9.0',
  },
  {
    License: 'Apache Software License',
    Name: 'msgpack',
    Version: '1.0.7',
  },
  {
    License: 'Apache Software License',
    Name: 'openai',
    Version: '1.8.0',
  },
  {
    License: 'Apache Software License',
    Name: 'requests',
    Version: '2.31.0',
  },
  {
    License: 'Apache Software License',
    Name: 'tornado',
    Version: '6.4',
  },
  {
    License: 'Apache Software License; BSD License',
    Name: 'python-dateutil',
    Version: '2.8.2',
  },
  {
    License: 'Apache Software License; MIT License',
    Name: 'sniffio',
    Version: '1.3.0',
  },
  {
    License: 'BSD 3-Clause',
    Name: 'matplotlib-inline',
    Version: '0.1.6',
  },
  {
    License: 'BSD License',
    Name: 'Flask',
    Version: '3.0.2',
  },
  {
    License: 'BSD License',
    Name: 'Jinja2',
    Version: '3.1.3',
  },
  {
    License: 'BSD License',
    Name: 'MarkupSafe',
    Version: '2.1.5',
  },
  {
    License: 'BSD License',
    Name: 'Werkzeug',
    Version: '3.0.1',
  },
  {
    License: 'BSD License',
    Name: 'click',
    Version: '8.1.7',
  },
  {
    License: 'BSD License',
    Name: 'comm',
    Version: '0.2.1',
  },
  {
    License: 'BSD License',
    Name: 'decorator',
    Version: '5.1.1',
  },
  {
    License: 'BSD License',
    Name: 'httpcore',
    Version: '1.0.2',
  },
  {
    License: 'BSD License',
    Name: 'httpx',
    Version: '0.26.0',
  },
  {
    License: 'BSD License',
    Name: 'idna',
    Version: '3.6',
  },
  {
    License: 'BSD License',
    Name: 'ipykernel',
    Version: '6.29.0',
  },
  {
    License: 'BSD License',
    Name: 'ipython',
    Version: '8.20.0',
  },
  {
    License: 'BSD License',
    Name: 'itsdangerous',
    Version: '2.1.2',
  },
  {
    License: 'BSD License',
    Name: 'jupyter_client',
    Version: '8.6.0',
  },
  {
    License: 'BSD License',
    Name: 'jupyter_core',
    Version: '5.7.1',
  },
  {
    License: 'BSD License',
    Name: 'memory-profiler',
    Version: '0.61.0',
  },
  {
    License: 'BSD License',
    Name: 'nest-asyncio',
    Version: '1.5.9',
  },
  {
    License: 'BSD License',
    Name: 'prettytable',
    Version: '3.10.0',
  },
  {
    License: 'BSD License',
    Name: 'prompt-toolkit',
    Version: '3.0.43',
  },
  {
    License: 'BSD License',
    Name: 'psutil',
    Version: '5.9.7',
  },
  {
    License: 'BSD License',
    Name: 'python-dotenv',
    Version: '1.0.1',
  },
  {
    License: 'BSD License',
    Name: 'traitlets',
    Version: '5.14.1',
  },
  {
    License: 'BSD License',
    Name: 'uvicorn',
    Version: '0.26.0',
  },
  {
    License: 'BSD License; GNU Library or Lesser General Public License (LGPL)',
    Name: 'pyzmq',
    Version: '25.1.2',
  },
  {
    License: 'ISC License (ISCL)',
    Name: 'pexpect',
    Version: '4.9.0',
  },
  {
    License: 'ISC License (ISCL)',
    Name: 'ptyprocess',
    Version: '0.7.0',
  },
  {
    License: 'MIT',
    Name: 'geventhttpclient',
    Version: '2.0.11',
  },
  {
    License: 'MIT License',
    Name: 'Brotli',
    Version: '1.1.0',
  },
  {
    License: 'MIT License',
    Name: 'ConfigArgParse',
    Version: '1.7',
  },
  {
    License: 'MIT License',
    Name: 'Flask-Cors',
    Version: '4.0.0',
  },
  {
    License: 'MIT License',
    Name: 'Flask-Login',
    Version: '0.6.3',
  },
  {
    License: 'MIT License',
    Name: 'annotated-types',
    Version: '0.6.0',
  },
  {
    License: 'MIT License',
    Name: 'anyio',
    Version: '4.2.0',
  },
  {
    License: 'MIT License',
    Name: 'blinker',
    Version: '1.7.0',
  },
  {
    License: 'MIT License',
    Name: 'charset-normalizer',
    Version: '3.3.2',
  },
  {
    License: 'MIT License',
    Name: 'debugpy',
    Version: '1.8.0',
  },
  {
    License: 'MIT License',
    Name: 'executing',
    Version: '2.0.1',
  },
  {
    License: 'MIT License',
    Name: 'gevent',
    Version: '23.9.1',
  },
  {
    License: 'MIT License',
    Name: 'greenlet',
    Version: '3.0.3',
  },
  {
    License: 'MIT License',
    Name: 'h11',
    Version: '0.14.0',
  },
  {
    License: 'MIT License',
    Name: 'jedi',
    Version: '0.19.1',
  },
  {
    License: 'MIT License',
    Name: 'locust',
    Version: '2.22.0',
  },
  {
    License: 'MIT License',
    Name: 'parso',
    Version: '0.8.3',
  },
  {
    License: 'MIT License',
    Name: 'pip',
    Version: '23.3.2',
  },
  {
    License: 'MIT License',
    Name: 'pip-licenses',
    Version: '4.3.4',
  },
  {
    License: 'MIT License',
    Name: 'platformdirs',
    Version: '4.1.0',
  },
  {
    License: 'MIT License',
    Name: 'pure-eval',
    Version: '0.2.2',
  },
  {
    License: 'MIT License',
    Name: 'pydantic',
    Version: '2.5.3',
  },
  {
    License: 'MIT License',
    Name: 'pydantic-settings',
    Version: '2.1.0',
  },
  {
    License: 'MIT License',
    Name: 'pydantic_core',
    Version: '2.14.6',
  },
  {
    License: 'MIT License',
    Name: 'roundrobin',
    Version: '0.0.4',
  },
  {
    License: 'MIT License',
    Name: 'setuptools',
    Version: '65.5.0',
  },
  {
    License: 'MIT License',
    Name: 'six',
    Version: '1.16.0',
  },
  {
    License: 'MIT License',
    Name: 'stack-data',
    Version: '0.6.3',
  },
  {
    License: 'MIT License',
    Name: 'urllib3',
    Version: '2.2.0',
  },
  {
    License: 'MIT License',
    Name: 'wcwidth',
    Version: '0.2.13',
  },
  {
    License: 'MIT License; Mozilla Public License 2.0 (MPL 2.0)',
    Name: 'tqdm',
    Version: '4.66.1',
  },
  {
    License: 'Mozilla Public License 2.0 (MPL 2.0)',
    Name: 'certifi',
    Version: '2023.11.17',
  },
  {
    License: 'Python Software Foundation License',
    Name: 'typing_extensions',
    Version: '4.9.0',
  },
  {
    License: 'Zope Public License',
    Name: 'zope.event',
    Version: '5.0',
  },
  {
    License: 'Zope Public License',
    Name: 'zope.interface',
    Version: '6.1',
  },
];

import React from 'react';
import Toggle from 'react-toggle';
import 'react-toggle/style.css';
import {
  DEFAULT_KEYWORD_INSTRUCTIONS,
  DEFAULT_QUOTE_INSTRUCTIONS,
  DEFAULT_SECTION_INSTRUCTIONS,
  EXTENDED_VERSION,
} from '../../helpers/constants';
import Icons from '../../components/icons';
import styles from './Options.module.css';

interface Props {
  showVerificationResult?: boolean;
  showExplanation?: boolean;
  playSounds?: boolean;
  clientSecret?: string;
  keywordInstructions?: string;
  sectionInstructions?: string;
  quoteInstructions?: string;
  modelVersion?: string;
  setShowVerificationResult: (show: boolean) => void;
  setShowExplanation: (show: boolean) => void;
  setPlaySounds: (play: boolean) => void;
  setClientSecret?: (clientSecret: string) => void;
  setKeywordInstructions?: (keywordInstructions: string) => void;
  setSectionInstructions?: (sectionInstructions: string) => void;
  setQuoteInstructions?: (quoteInstructions: string) => void;
  setModelVersion?: (modelVersion: string) => void;
}

const Options: React.FC<Props> = ({
  showVerificationResult,
  showExplanation,
  playSounds,
  clientSecret,
  keywordInstructions,
  sectionInstructions,
  quoteInstructions,
  modelVersion,
  setShowVerificationResult,
  setShowExplanation,
  setPlaySounds,
  setClientSecret,
  setKeywordInstructions,
  setSectionInstructions,
  setQuoteInstructions,
  setModelVersion,
}: Props) => {
  return (
    <div className={styles.container}>
      <h1 className={styles.h1}>Settings</h1>
      <div className={styles.toggleContainer}>
        <div className={styles.toggleRow}>
          <div className={styles.toggleText}>
            <b>Verification status</b>
            <span>
              Show whether or not a statement is supported by Wikipedia.
            </span>
          </div>
          <Toggle
            checked={!!showVerificationResult}
            onChange={(e) => {
              setShowVerificationResult(e.target.checked);
              // If we don't show the verification result, we also don't want to show any
              // explanations
              if (!e.target.checked) {
                setShowExplanation(false);
              }
            }}
          />
        </div>
        <div
          className={[
            styles.toggleRow,
            styles.dependentToggleRow,
            ...[!showVerificationResult ? [styles.disabledToggleRow] : []],
          ].join(' ')}
        >
          <div className={styles.toggleText}>
            <b>AI-generated explanation</b>
            <span>Use AI to clarify Wikipedia stance on the statement.</span>
          </div>
          <Toggle
            disabled={!showVerificationResult}
            checked={!!showExplanation && !!showVerificationResult}
            onChange={(e) => {
              setShowExplanation(e.target.checked);
            }}
          />
        </div>
        <div className={styles.toggleRow}>
          <div className={styles.toggleText}>
            <b>Sounds</b>
            <span>
              Play sounds to inform you about the progress of the verification.
            </span>
          </div>
          <Toggle
            checked={!!playSounds}
            onChange={(e) => {
              setPlaySounds(e.target.checked);
            }}
          />
        </div>
      </div>
      {EXTENDED_VERSION && (
        <div className={styles.extendedVersionContainer}>
          <div className={styles.inputRow}>
            <label htmlFor="clientSecret">Client Secret</label>
            <input
              type="text"
              id="clientSecret"
              placeholder="Client Secret"
              value={clientSecret}
              onChange={(e) => {
                setClientSecret?.(e.target.value);
              }}
            />
          </div>
          <div className={styles.inputRow}>
            <label htmlFor="keywordInstructions">Keyword Instructions</label>
            <textarea
              id="keywordInstructions"
              value={keywordInstructions || DEFAULT_KEYWORD_INSTRUCTIONS}
              onChange={(e) => {
                setKeywordInstructions?.(e.target.value);
              }}
            />
          </div>
          <div className={styles.inputRow}>
            <label htmlFor="sectionInstructions">Section Instructions</label>
            <textarea
              id="sectionInstructions"
              value={sectionInstructions || DEFAULT_SECTION_INSTRUCTIONS}
              onChange={(e) => {
                setSectionInstructions?.(e.target.value);
              }}
            />
          </div>
          <div className={styles.inputRow}>
            <label htmlFor="quoteInstructions">Quote Instructions</label>
            <textarea
              id="quoteInstructions"
              value={quoteInstructions || DEFAULT_QUOTE_INSTRUCTIONS}
              onChange={(e) => {
                setQuoteInstructions?.(e.target.value);
              }}
            />
          </div>
          <div className={styles.inputRow}>
            <label htmlFor="modelVersion">Model Version</label>
            <select
              id="modelVersion"
              value={modelVersion || 'gpt-3.5-turbo-0125'}
              onChange={(e) => {
                setModelVersion?.(e.target.value);
              }}
            >
              <option value="gpt-3.5-turbo-0125">gpt-3.5-turbo-0125</option>
              <option value="gpt-4">gpt-4</option>
              <option value="gpt-4-turbo-preview">gpt-4-turbo-preview</option>
            </select>
          </div>
        </div>
      )}

      <h2 className={styles.h2}>Explanation of verification statuses</h2>
      <div className={styles.statusExplanation}>
        <div>
          <div className={styles.iconRow}>
            <Icons.Success
              className={[styles.icon, styles.correct].join(' ')}
            />
            <div className={[styles.explanation, styles.correct].join(' ')}>
              This article seems to back it up.
            </div>
          </div>
          <div className={styles.statusExplanationText}>
            Citation Needed has found information on Wikipedia that supports the
            statement you are verifying. You can read more to understand how by
            accessing the article and reading the article’s cited sources.
          </div>
        </div>
        <div>
          <div className={styles.iconRow}>
            <Icons.Alert
              className={[styles.icon, styles.partially_correct].join(' ')}
            />
            <div
              className={[styles.explanation, styles.partially_correct].join(
                ' '
              )}
            >
              This article doesn’t quite settle it.
            </div>
          </div>
          <div className={styles.statusExplanationText}>
            Citation Needed has found relevant information on Wikipedia that may
            partially support the statement, though not every aspect of the
            statement seems to be supported. You will want to do more research
            to check the rest of the statement.
          </div>
        </div>
        <div>
          <div className={styles.iconRow}>
            <Icons.Error
              className={[styles.icon, styles.incorrect].join(' ')}
            />
            <div className={[styles.explanation, styles.incorrect].join(' ')}>
              This article seems to contradict it.
            </div>
          </div>
          <div className={styles.statusExplanationText}>
            Citation Needed has found information on Wikipedia that seems to
            directly contradict the statement. You can read more to understand
            why it may be inaccurate.
          </div>
        </div>
        <div>
          <div className={styles.iconRow}>
            <Icons.InfoFilled
              className={[styles.icon, styles.no_result].join(' ')}
            />
            <div className={[styles.explanation, styles.no_result].join(' ')}>
              This article may be related.
            </div>
          </div>
          <div className={styles.statusExplanationText}>
            Citation Needed found an article that may be related to the topic,
            but nothing specific to the statement.
          </div>
        </div>
        <div>
          <div className={styles.iconRow}>
            <Icons.InfoFilled
              className={[styles.icon, styles.no_result].join(' ')}
            />
            <div className={[styles.explanation, styles.no_result].join(' ')}>
              No relevant articles found.
            </div>
          </div>
          <div className={styles.statusExplanationText}>
            Citation Needed could not find any Wikipedia articles relevant to
            the statement. This information may not be on Wikipedia, possibly
            because it is not notable encyclopedic content, or Wikipedia hasn’t
            been updated yet.
          </div>
        </div>
        <div>
          <div className={styles.iconRow}>
            <Icons.InfoFilled
              className={[styles.icon, styles.error].join(' ')}
            />
            <div className={[styles.explanation, styles.error].join(' ')}>
              Something went wrong. Please try again.
            </div>
          </div>
          <div className={styles.statusExplanationText}>
            One of the services we use (such as the Wikipedia search API) may
            not be functioning properly, preventing Citation Needed from getting
            results.
          </div>
        </div>
      </div>
      <h2 className={styles.h2}>License information</h2>
      <div>
        <a
          href="https://gitlab.wikimedia.org/repos/future-audiences/citation-needed-chrome-extension/-/blob/main/dependencies.json"
          className={styles.link}
        >
          Frontend dependencies
        </a>
      </div>
      <div>
        <a
          href="https://gitlab.wikimedia.org/repos/future-audiences/citation-needed-api/-/blob/main/dependencies.json"
          className={styles.link}
        >
          Backend dependencies
        </a>
      </div>
    </div>
  );
};

export default Options;

export class PersistentOptions extends React.Component {
  state = {
    showVerificationResult: false,
    showExplanation: false,
    playSounds: false,
    clientSecret: undefined,
    keywordInstructions: undefined,
    sectionInstructions: undefined,
    quoteInstructions: undefined,
    modelVersion: undefined,
  };

  componentDidMount(): void {
    chrome.storage?.local.get(
      [
        'showVerificationResult',
        'showExplanation',
        'playSounds',
        'clientSecret',
        'keywordInstructions',
        'sectionInstructions',
        'quoteInstructions',
        'modelVersion',
      ],
      (result) => {
        // Default set showVerificationResult and showExplanation to true, if they are undefined
        // Store the result in the local storage
        if (result.showVerificationResult === undefined) {
          result.showVerificationResult = true;
          chrome.storage.local.set({ showVerificationResult: true });
        }
        if (result.showExplanation === undefined) {
          result.showExplanation = true;
          chrome.storage.local.set({ showExplanation: true });
        }
        // Default set playSounds to false, if it is undefined
        // Store the result in the local storage
        if (result.playSounds === undefined) {
          result.playSounds = false;
          chrome.storage.local.set({ playSounds: false });
        }

        this.setState({
          showVerificationResult: result.showVerificationResult,
          showExplanation: result.showExplanation,
          playSounds: result.playSounds,
          clientSecret: result.clientSecret,
          keywordInstructions: result.keywordInstructions,
          sectionInstructions: result.sectionInstructions,
          quoteInstructions: result.quoteInstructions,
          modelVersion: result.modelVersion,
        });
      }
    );
  }

  render() {
    return (
      <Options
        showVerificationResult={this.state.showVerificationResult}
        showExplanation={this.state.showExplanation}
        playSounds={this.state.playSounds}
        clientSecret={this.state.clientSecret}
        keywordInstructions={this.state.keywordInstructions}
        sectionInstructions={this.state.sectionInstructions}
        quoteInstructions={this.state.quoteInstructions}
        modelVersion={this.state.modelVersion}
        setShowVerificationResult={(showVerificationResult) => {
          this.setState({ showVerificationResult });
          chrome.storage?.local.set({ showVerificationResult });
        }}
        setShowExplanation={(showExplanation) => {
          this.setState({ showExplanation });
          chrome.storage?.local.set({ showExplanation });
        }}
        setPlaySounds={(playSounds) => {
          this.setState({ playSounds });
          chrome.storage?.local.set({ playSounds });
        }}
        setClientSecret={(clientSecret) => {
          this.setState({ clientSecret });
          chrome.storage?.local.set({ clientSecret });
        }}
        setKeywordInstructions={(keywordInstructions) => {
          this.setState({ keywordInstructions });
          chrome.storage?.local.set({ keywordInstructions });
        }}
        setSectionInstructions={(sectionInstructions) => {
          this.setState({ sectionInstructions });
          chrome.storage?.local.set({ sectionInstructions });
        }}
        setQuoteInstructions={(quoteInstructions) => {
          this.setState({ quoteInstructions });
          chrome.storage?.local.set({ quoteInstructions });
        }}
        setModelVersion={(modelVersion) => {
          this.setState({ modelVersion });
          chrome.storage?.local.set({ modelVersion });
        }}
      />
    );
  }
}

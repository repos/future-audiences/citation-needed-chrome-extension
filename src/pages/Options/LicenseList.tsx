import { useState } from 'react';
import styles from './LicenseList.module.css';
import React from 'react';
import { FrontendLiceces } from './frontendLicences';
import { BackendLicences } from './backendLicences';

export const LicenseList = () => {
  const [isExtended, setIsExtended] = useState<boolean>(true);
  return (
    <div className={styles.container}>
      <h1 onClick={() => setIsExtended(!isExtended)}>
        License Information{' '}
        <span className={styles.chevron}>{isExtended ? '▼' : '▲'}</span>
      </h1>
      <div className={isExtended ? styles.listExtended : styles.listHidden}>
        <h2>Frontend Dependencies</h2>
        <ul className={styles.list}>
          {FrontendLiceces.map((license) => (
            <li key={license.name}>
              <div className={styles.entry}>
                <b>{license.name}</b>
                {license.licenseType}
                {license.licensePeriod ? ` (${license.licensePeriod})` : ''}
              </div>
              <a href={license.link} target="_blank" rel="noreferrer">
                {license.link}
              </a>
            </li>
          ))}
        </ul>
        <h2>API Dependencies</h2>
        <ul className={styles.list}>
          {BackendLicences.map((license) => (
            <li key={license.Name}>
              <div className={styles.entry}>
                <b>{license.Name}</b>
                {license.License}
              </div>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

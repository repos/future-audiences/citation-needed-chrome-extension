function getTitle() {
    const twitterTitle = document.getElementsByName("twitter:title");
    const ogTitle = document.getElementsByName("og:title");
    const title = document.title;
    if (twitterTitle.length > 0) {
      return twitterTitle[0].getAttribute("content");
    } else if (ogTitle.length > 0) {
      return ogTitle[0].getAttribute("content");
    } else {
      return title;
    }
  }
  
  function getDescription() {
    const twitterDescription = document.getElementsByName("twitter:description");
    const ogDescription = document.getElementsByName("og:description");
    if (twitterDescription.length > 0) {
      return twitterDescription[0].getAttribute("content");
    } else if (ogDescription.length > 0) {
      return ogDescription[0].getAttribute("content");
    } else {
      return null;
    }
  }
  
  function getDate() {
    // meta elements with property="article:published_time" or property="article:modified_time"
    const publishedDate = document.querySelectorAll('meta[property="article:published_time"]');
    const updatedDate = document.querySelectorAll('meta[property="article:modified_time"]');
    const publishedDate2 = document.getElementsByName("dcterms.created");
    const updatedDate2 = document.getElementsByName("dcterms.modified");
  
    if (publishedDate.length > 0) {
      return publishedDate[0].getAttribute("content");
    } else if (updatedDate.length > 0) {
      return updatedDate[0].getAttribute("content");
    } else if (publishedDate2.length > 0) {
      return publishedDate2[0].getAttribute("content");
    } else if (updatedDate2.length > 0) {
      return updatedDate2[0].getAttribute("content");
    }
    return null;
  }
  
  function getContext() {
    // should return a string of the format "$title - $description at $date"
    const title = getTitle();
    const description = getDescription();
    const date = getDate();
    let context = "";
    if (title) {
        context += title;
        if (description) {
            context += ` - ${description}`;
        }
    } else if (description) {
        context += description;
    }
    if (date) {
        context += ` at ${date}`;
    }
    return context;
  }

async function addMouseUpHandler(tabId, tabUrl) {
    try {
        if (!window.wmf_citationNeededInit) {
            document.addEventListener('mouseup', () => {
                const selection = getSelection().toString().trim();
                if (selection && selection.length > 0) {
                chrome.runtime
                    .sendMessage({
                    type: 'selection',
                    tabId: tabId,
                    tabUrl: tabUrl,
                    selection,
                    context: getContext(),
                    })
                    .catch((error) => {
                    console.error(error, 'Failed to send selection. Trying again');
                    setTimeout(() => {
                        chrome.runtime
                        .sendMessage({
                            type: 'selection',
                            tabId: tabId,
                            tabUrl: tabUrl,
                            selection,
                            context: getContext(),
                        })
                        .catch((error) => {
                            console.error(error, 'Failed to send selection.');
                        });
                    }, 1000);
                    });
                }
            });
            // don't rerun this code
            window.wmf_citationNeededInit = true;
        }
      return `onMouseUp handler added.`;
    } catch (error) {
      return `${error}`;
    }
}

// expose these functions for later use via execute script commands
window.addMouseUpHandler = addMouseUpHandler;
window.getContext = getContext;

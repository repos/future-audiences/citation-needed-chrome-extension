
/**
 * 
 * @param {*} tab the tab we are selecting text from
 * @param {*} overrideResult whether we should override the currently selected text in the sidepanel (or defer it for later)
 */
function handleSelection(tab, overrideResult = false) {
  chrome.scripting
    .executeScript({
      target: { tabId: tab.id },
      func: () => {
        return {
          selection: getSelection().toString().trim(),
          context: getContext(),
        };
      },
    })
    .then((executionResult) => {
      const [{ result: { selection, context }}] = executionResult;
      if (selection && selection.length > 0) {
        chrome.runtime
          .sendMessage({
            type: 'selection',
            tabId: tab.id,
            tabUrl: tab.url,
            selection,
            context,
            overrideResult
          })
          .catch((error) => {
            setTimeout(() => {
              chrome.runtime
                .sendMessage({
                  type: 'selection',
                  tabId: tab.id,
                  tabUrl: tab.url,
                  selection,
                  context,
                  overrideResult
                })
                .catch((error) => {
                  console.error(error, 'Failed to send selection.');
                });
            }, 500);
          });
      }
    });
}

const BANNED_DOMAINS = ['chromewebstore.google.com'];
const isBannedUrl = (url) => {
  if (!url || url.includes('chrome://')) {
    return true;
  }

  for (const domain of BANNED_DOMAINS) {
    const urlObj = new URL(url);
    if (urlObj.hostname === domain) {
      return true;
    }
  }
  return false;
};

// // Allows users to open the side panel by clicking on the action toolbar icon
// // Enable this part if we want to use the sidepanel:
chrome.sidePanel
  .setPanelBehavior({ openPanelOnActionClick: false })
  .catch((error) => console.error(error));

chrome.action.onClicked.addListener((tab) => {
  chrome.sidePanel.open({ windowId: tab.windowId, tabId: tab.id });

  handleChromeTabChange();

  if (!isBannedUrl(tab.url)) {
    setTimeout(() => {
      handleSelection(tab);
    }, 100);
  }
});

const handleChromeTabChange = () => {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    var currTab = tabs[0];
    if (currTab) {
      // If there is no url, we're on a page where we can't read it - so treat is as one of the banned urls
      const fallbackSafeUrl =
        currTab.url?.split('#')[0].split('?')[0] || 'chrome://';
      chrome.runtime
        .sendMessage({
          type: 'urlChange',
          url: fallbackSafeUrl,
        })
        .catch((error) => {
          console.log(
            `Encountered error while updating the current url: ${error}. Retrying.`
          );
          setTimeout(() => {
            chrome.runtime
              .sendMessage({
                type: 'urlChange',
                url: fallbackSafeUrl,
              })
              .catch((error) => {
                console.log(`Failed to update the current url: ${error}.`);
              });
          }, 500);
        });
      if (isBannedUrl(currTab?.url)) {
        return;
      }
      chrome.scripting.executeScript({target: {tabId: currTab.id}, files: ['contentScript.bundle.js']}).then(() => {
        chrome.scripting.executeScript({
          target: { tabId: currTab.id },
          func: (...args) => {
            window.addMouseUpHandler(...args);
          },
          args: [currTab.id, currTab.url],
        });
      });
    }
  });
};

chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
  handleChromeTabChange();
});

chrome.tabs.onActivated.addListener(function (activeInfo) {
  handleChromeTabChange();
});

// Handle the initial load
try {
  handleChromeTabChange();
} catch (error) {
  console.error(error);
}


chrome.contextMenus.create(
   {
    id: 'select-sentence',
    title: 'Wikipedia Citation Needed',
    contexts: ['selection'],
  }
);

chrome.contextMenus.onClicked.addListener(
  function(info, tab) {
    if (info.menuItemId === 'select-sentence') {
      chrome.sidePanel.open({ windowId: tab.windowId, tabId: tab.id });
      handleSelection(tab, true);
    }
  }
);
import React, { useEffect, useState } from 'react';
import SidepanelHeader from '../../components/SidepanelHeader';
import StartText from '../../components/StartText';
import styles from './Sidepanel.module.css';
import './Sidepanel.css';
import {
  MAX_SELECTION_LENGTH,
  MAX_SENTENCE_LENGTH,
} from '../../helpers/selectionHelpers';
import SelectedText from '../../components/SelectedText';
import WikipediaArticle, {
  WikipediaArticleProps,
} from '../../components/WikipediaArticle';
import SidepanelFooter from '../../components/SidepanelFooter';
import StyledButton from '../../components/StyledButton';
import {
  BASE_URL,
  DEFAULT_KEYWORD_INSTRUCTIONS,
  DEFAULT_QUOTE_INSTRUCTIONS,
  DEFAULT_SECTION_INSTRUCTIONS,
  EXTENDED_BASE_URL,
  EXTENDED_VERSION,
  InteractionSteps,
  NUM_VERIFICATIONS_BEFORE_DONATION_BANNER,
} from '../../helpers/constants';
import Icons from '../../components/icons';
import { VerificationResult } from '../../helpers/verificationResults';
import VerificationStatusBlock from '../../components/VerificationStatusBlock';
import { v4 as uuidv4 } from 'uuid';
import { hashUserId } from '../../helpers/crypto';
import { classifyUrl, isBannedUrl } from '../../helpers/urlClassifier';
import { DonationBanner } from '../../components/DonationBanner';
import BadUrlText from '../../components/BadUrlText';
import { SurveyBanner } from '../../components/SurveyBanner';

export interface APIResponse {
  result: VerificationResult;
  explanation: string;
  article?: WikipediaArticleProps;
  current_step?: string;
}

export interface OptionsForSidepanel {
  isUsageAccepted: boolean;
  showVerificationResult: boolean;
  showExplanation: boolean;
  keywordInstructions?: string;
  sectionInstructions?: string;
  quoteInstructions?: string;
  modelVersion?: string;
  clientSecret?: string;
  userId: string;
  setIsUsageAccepted: (isUsageAccepted: boolean) => void;
  showDonationBanner?: boolean;
  showSurveyBanner?: boolean;
  updateNumberOfVerifications: () => void;
  hideDonationBanner: () => void;
  hideSurveyBanner: () => void;
}

export interface SidepanelProps extends OptionsForSidepanel {
  options?: string[];
  selection?: string;
  showFullSelection?: boolean;
  isVerifying?: boolean;
  verificationResult?: VerificationResult;
  onVerify: (selection: string) => void;
  setSelectedText: (text: string) => void;
  explanation?: string;
  page?: WikipediaArticleProps;
  previousSteps?: string[];
  currentStep?: string;
  useTextArea?: boolean;
  requestId?: string;
  hasErrored?: boolean;
  onReset: (useSelectionUpdateDuringVerification: boolean) => void;
  currentUrl?: string;
  isBadUrl?: boolean;
  showDonationBanner?: boolean;
  surveyBannerShownAt?: number;
}

const KnownErrorMessages = {
  API_DISABLED:
    'This service is currently not available. Please try again later.',
  USER_BLOCKED:
    'You have made too many requests and are temporarily blocked. Please try again later.',
};

const showVerificationBlockForStatus = {
  [VerificationResult.CORRECT]: true,
  [VerificationResult.INCORRECT]: true,
  [VerificationResult.PARTIALLY_CORRECT]: true,
  [VerificationResult.QUOTE_FOUND]: false,
  [VerificationResult.NO_ARTICLES]: true,
  [VerificationResult.NO_RESULT]: true,
  [VerificationResult.ERROR]: true,
  [VerificationResult.UNKNOWN]: false,
};

const playSoundIfEnabled = (soundFile: string) => {
  if (chrome && chrome.storage) {
    chrome.storage.local.get('playSounds').then((result) => {
      if (result.playSounds) {
        const audio = new Audio(soundFile);
        audio.play();
      }
    });
  }
};
export const Sidepanel = (props: SidepanelProps) => {
  const [isUsageAcceptedChecked, setIsUsageAcceptedChecked] = useState<boolean>(
    props.isUsageAccepted
  );
  const [selectedOptionIndex, setSelectedOptionIndex] = useState<number>(0);
  const [showFullSelection, setShowFullSelection] = useState<boolean>(false);

  const showCancelButton =
    !props.page &&
    (!!props.verificationResult || props.isVerifying || !!props.selection);

  const mainContent = props.isBadUrl ? (
    <BadUrlText />
  ) : !props.page && !props.selection && !props.options ? (
    <StartText />
  ) : (
    <>
      <SelectedText
        isVerifying={props.isVerifying || false}
        options={props.options}
        selectedOptionIndex={selectedOptionIndex}
        showOptions={!props.isVerifying && !props.verificationResult}
        selection={props.selection}
        showFullSelection={showFullSelection}
        allowSelectionMinimization={!!props.page || !!props.verificationResult}
        setSelectedText={props.setSelectedText}
        setShowFullSelection={setShowFullSelection}
        setSelectedOptionIndex={setSelectedOptionIndex}
        useTextArea={
          props.useTextArea && !props.isVerifying && !props.verificationResult
        }
        verificationResult={props.verificationResult}
        showVerificationResult={props.showVerificationResult}
      />
      {!props.isUsageAccepted && (
        <div className={styles.usageNotAccepted}>
          <input
            type="checkbox"
            className={styles.checkbox}
            checked={!!isUsageAcceptedChecked}
            onClick={() => setIsUsageAcceptedChecked(!isUsageAcceptedChecked)}
          />
          <span>
            I agree to the{' '}
            <a
              href="https://foundation.wikimedia.org/wiki/Policy:Terms_of_Use"
              target="_blank"
            >
              terms of use
            </a>{' '}
            and acknowledge the{' '}
            <a
              href="https://wikimediafoundation.org/privacy-policy/"
              target="_blank"
            >
              privacy policy
            </a>
            .
          </span>
        </div>
      )}

      {!props.page && props.isVerifying && (
        <div className={styles.currentStepContainer}>
          {InteractionSteps.map((step, index) => {
            const isFinishedStep = props.previousSteps?.includes(step);
            const isActiveStep =
              props.currentStep === step || (!props.currentStep && index === 0);
            const isFutureStep = !isFinishedStep && !isActiveStep;

            return (
              <div key={index} className={styles.interactionStep}>
                <div
                  className={[
                    styles.interactionStepIndicator,
                    isActiveStep ? styles.activeInteractionStepIndicator : '',
                  ].join(' ')}
                >
                  {isFinishedStep && <Icons.Checkmark />}
                  {isActiveStep && <Icons.Spinner />}
                  {isFutureStep && <Icons.FutureStep />}
                </div>
                <div
                  className={[
                    styles.interactionStepText,
                    isActiveStep
                      ? styles.activeInteractionStep
                      : isFinishedStep
                      ? styles.previousInteractionStep
                      : styles.futureInteractionStep,
                  ].join(' ')}
                >
                  {step}
                </div>
              </div>
            );
          })}
        </div>
      )}
      {props.verificationResult &&
        showVerificationBlockForStatus[props.verificationResult] && (
          <VerificationStatusBlock
            result={props.verificationResult}
            generatedExplanation={props.explanation}
            showExplanation={props.showExplanation}
            showVerificationResult={props.showVerificationResult}
          />
        )}
      {!props.page && props.isVerifying && (
        <div className={styles.explanation}>&nbsp;</div>
      )}
      {props.page && (
        <WikipediaArticle {...props.page} currentUrl={props.currentUrl} />
      )}
      {/* {props.verificationResult === VerificationResult.ERROR &&
          props.explanation && (
            <div className={styles.explanationContainer}>
              <div className={styles.explanationHeader}>An Error occured</div>
              <div className={styles.explanation}>{props.explanation}</div>
            </div>
          )} */}
      {props.verificationResult === VerificationResult.NO_ARTICLES && (
        <div className={styles.noArticlesTextContainer}>
          <div>
            Wikipedia is a collaborative platform where volunteers create
            articles.
          </div>
          <div>
            It's possible this topic hasn't been covered, isn't notable, or
            lacks reliable sources to be published on Wikipedia.
          </div>
        </div>
      )}
      {props.page &&
        (!props.currentUrl ||
          !props.page.page_url.startsWith(props.currentUrl)) && (
          <div className={styles.buttonSeperator}>or</div>
        )}
      {!props.isVerifying ? (
        <div className={styles.verifyButtonContainer}>
          {showCancelButton ? (
            <StyledButton
              primary={false}
              onClick={() => props.onReset(false)}
              width={70}
            >
              Cancel
            </StyledButton>
          ) : (
            <></>
          )}
          <StyledButton
            primary={!props.page}
            width={240}
            onClick={() => {
              if (props.verificationResult && !props.hasErrored) {
                props.onReset(true);
                return;
              }

              props.onVerify(
                props.options
                  ? props.options[selectedOptionIndex]
                  : props.selection || ''
              );
            }}
            disabled={
              (!props.isUsageAccepted && !isUsageAcceptedChecked) ||
              !props.selection ||
              props.selection.length > MAX_SELECTION_LENGTH
            }
          >
            {props.hasErrored
              ? 'Verify statement again'
              : props.verificationResult
              ? 'Verify another statement'
              : 'Verify statement'}
          </StyledButton>
        </div>
      ) : (
        <></>
      )}
    </>
  );
  return (
    <div className={styles.container}>
      {props.selection && (
        <SidepanelHeader
          verificationResult={props.verificationResult}
          isVerifying={props.isVerifying}
        />
      )}
      <div className={styles.mainContent}>{mainContent}</div>
      {props.showSurveyBanner &&
        props.verificationResult &&
        props.verificationResult !== VerificationResult.ERROR && (
          <SurveyBanner hideSurveyBanner={props.hideSurveyBanner} />
        )}

      {props.showDonationBanner &&
        props.verificationResult &&
        props.verificationResult !== VerificationResult.ERROR && (
          <DonationBanner hideDonationBanner={props.hideDonationBanner} />
        )}
      <SidepanelFooter
        selection={props.selection}
        article={props.page}
        explanation={props.explanation}
        requestId={props.requestId}
      />
    </div>
  );
};

export const SidepanelWrapped = (props: SidepanelProps & { style: any }) => {
  console.log(props);
  const otherProps = { ...props };
  delete otherProps.style;
  return (
    <div
      style={{
        width: 600,
        backgroundColor: '#f0f0f0',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: '32px 0',
      }}
    >
      <div
        className={styles.wrapper}
        style={{
          ...(props.style || {}),
          borderRadius: '16px',
          overflow: 'hidden',
        }}
      >
        <Sidepanel {...otherProps} />
      </div>
    </div>
  );
};

export const SidepanelWrappedWithState = (props: OptionsForSidepanel) => {
  const [options, setOptions] = useState<string[] | undefined>();
  const [selection, setSelection] = useState<string>('');
  const [context, setContext] = useState<string>('');
  const [selectionDuringVerification, setSelectionDuringVerification] =
    useState<string>('');
  const [contextDuringVerification, setContextDuringVerification] =
    useState<string>('');

  const [siteCategory, setSiteCategory] = useState<string>('');
  const [nextSiteCategory, setNextSiteCategory] = useState<string>('');
  const [useTextArea, setUseTextArea] = useState<boolean>(false);
  const [isVerifying, setIsVerifying] = useState<boolean>(false);
  const [hasErrored, setHasErrored] = useState<boolean>(false);
  const [verificationResult, setVerificationResult] = useState<
    VerificationResult | undefined
  >(undefined);
  const [explanation, setExplanation] = useState<string>('');
  const [article, setArticle] = useState<WikipediaArticleProps | undefined>(
    undefined
  );
  const [previousSteps, setPreviousSteps] = useState<string[]>([]);
  const [currentStep, setCurrentStep] = useState<string>('');
  const [requestId, setRequestId] = useState<string>('');
  const [currentUrl, setCurrentUrl] = useState<string>('');
  const [isBadUrl, setIsBadUrl] = useState<boolean>(false);

  useEffect(() => {
    const listener = (request: any) => {
      console.log('Received message:', JSON.stringify(request));
      if (request.type === 'selection') {
        if (request.overrideResult && verificationResult && !isVerifying) {
          // if a verification is done and we want to override the selection, we need to reset the state
          setPreviousSteps([]);
          setIsVerifying(false);
          setVerificationResult(undefined);
          setArticle(undefined);
          setSelection(request.selection);
          setContext(request.context);
          setUseTextArea(request.selection.length > MAX_SENTENCE_LENGTH);
          setSiteCategory(classifyUrl(request.tabUrl));
        } else if (!isVerifying && !verificationResult) {
          setSelection(request.selection);
          setContext(request.context);
          setUseTextArea(request.selection.length > MAX_SENTENCE_LENGTH);
          setSiteCategory(classifyUrl(request.tabUrl));
        } else {
          setSelectionDuringVerification(request.selection);
          setContextDuringVerification(request.context);
          setUseTextArea(request.selection.length > MAX_SENTENCE_LENGTH);
          setNextSiteCategory(request.tabUrl);
        }
      } else if (request.type === 'urlChange') {
        if (isBannedUrl(request.url)) {
          setIsBadUrl(true);
        } else {
          setIsBadUrl(false);
          setCurrentUrl(request.url);
        }
      }
    };
    chrome.runtime.onMessage.addListener(listener);
    return () => {
      chrome.runtime.onMessage.removeListener(listener);
    };
  }, [setSelection, isVerifying, verificationResult]);

  return (
    <div className={styles.wrapper}>
      <Sidepanel
        {...props}
        selection={selection}
        useTextArea={useTextArea}
        isVerifying={isVerifying && !article}
        hasErrored={hasErrored}
        isBadUrl={isBadUrl}
        verificationResult={verificationResult}
        explanation={explanation}
        page={article}
        requestId={requestId}
        options={options}
        currentStep={currentStep}
        previousSteps={previousSteps.slice(0, -1)}
        currentUrl={currentUrl}
        setSelectedText={(text: string) => {
          setSelection(text);
        }}
        onReset={(useSelectionUpdateDuringVerification: boolean) => {
          setHasErrored(false);
          setPreviousSteps([]);
          setCurrentStep('');
          setIsVerifying(false);
          setVerificationResult(undefined);
          setArticle(undefined);
          // Handle changes to the selection that happened during verification, if the reset was
          // intended to keep the selection
          if (!useSelectionUpdateDuringVerification) {
            setSelection('');
            setContext('');
            setSelectionDuringVerification('');
            setContextDuringVerification('');
            setSiteCategory('');
            setNextSiteCategory('');
            setCurrentUrl('');
          } else {
            setSelection(selectionDuringVerification);
            setSiteCategory(nextSiteCategory);
            setContext(contextDuringVerification);
            setSelectionDuringVerification('');
            setContextDuringVerification('');
            setNextSiteCategory('');
          }
        }}
        onVerify={async (selection: string) => {
          setHasErrored(false);
          setPreviousSteps([]);
          props.setIsUsageAccepted(true);
          setSelection(selection);
          setIsVerifying(true);
          setVerificationResult(undefined);
          setCurrentUrl('');

          // Prepare to receive data from the server as a stream
          const textDecorder = new TextDecoder();
          const handleError = (errorMessage?: string) => {
            setIsVerifying(false);
            setVerificationResult(VerificationResult.ERROR);
            setHasErrored(true);
            setExplanation(
              errorMessage || 'Something went wrong. Please try again.'
            );
          };

          const streamUrl =
            `${EXTENDED_VERSION ? EXTENDED_BASE_URL : BASE_URL}?` +
            new URLSearchParams(
              EXTENDED_VERSION
                ? {
                    selection,
                    context,
                    instructions_keywords:
                      props.keywordInstructions || DEFAULT_KEYWORD_INSTRUCTIONS,
                    instructions_sections:
                      props.sectionInstructions || DEFAULT_SECTION_INSTRUCTIONS,
                    instructions_quote:
                      props.quoteInstructions || DEFAULT_QUOTE_INSTRUCTIONS,
                    model_version: props.modelVersion || 'gpt-3.5-turbo-0125',
                  }
                : {
                    selection,
                    context,
                  }
            );
          const hashedUserId = props.userId
            ? await hashUserId(props.userId)
            : undefined;
          try {
            const response = await fetch(streamUrl, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'X-User-Id': hashedUserId || 'anonymous',
                'X-User-Secret': props.clientSecret || '',
                'X-Site-Category': siteCategory || '',
              },
            });
            setRequestId(response.headers.get('X-Request-Id') || '');
            if (response.status !== 200) {
              console.log(
                `Encountered a response with status ${response.status}`
              );
              response.arrayBuffer().then((buffer) => {
                console.log(`Going to try to decode ${buffer}`);
                const text = textDecorder.decode(buffer);
                console.log(`Decoded buffer is ${text}`);
                const textParts = text.split('\n').filter((part) => !!part);
                const data = JSON.parse(textParts[textParts.length - 1]);
                if (
                  data.detail &&
                  KnownErrorMessages[
                    data.detail as keyof typeof KnownErrorMessages
                  ]
                ) {
                  console.log(
                    'Error:',
                    data.detail,
                    KnownErrorMessages[
                      data.detail as keyof typeof KnownErrorMessages
                    ]
                  );
                  handleError(
                    KnownErrorMessages[
                      data.detail as keyof typeof KnownErrorMessages
                    ]
                  );
                } else {
                  handleError();
                }
              });
              return;
            }
            const readableStream = response.body;
            readableStream?.pipeTo(
              new WritableStream({
                write(chunk) {
                  try {
                    // Get data as json
                    const text = textDecorder.decode(chunk);
                    const textParts = text.split('\n').filter((part) => !!part);
                    textParts.forEach((textPart) => {
                      const data = JSON.parse(textPart) as APIResponse;
                      console.log('Received request data:', data);

                      if (data.result) {
                        if (data.result === VerificationResult.ERROR) {
                          setHasErrored(true);
                        } else {
                          props.updateNumberOfVerifications();
                        }
                        playSoundIfEnabled('wikimedia_1_sec.wav');
                        setVerificationResult(data.result);
                        setExplanation(data.explanation);
                        setIsVerifying(false);
                        setArticle(
                          data.article
                            ? {
                                ...data.article,
                                page_url: data?.article?.page_url.replace(
                                  'http://',
                                  'https://'
                                ),
                                section_url: data?.article?.section_url.replace(
                                  'http://',
                                  'https://'
                                ),
                              }
                            : undefined
                        );
                      } else if (data.current_step) {
                        setPreviousSteps((prev) => [
                          ...prev,
                          data.current_step!,
                        ]);
                        setCurrentStep(data.current_step);
                      }
                    });
                  } catch (error) {
                    console.log(
                      `Encountered an error while handling a response`
                    );
                    console.log(error);
                    handleError();
                  }
                },
              })
            );
          } catch (error) {
            console.log('Error while access the server:', error);
            handleError();
          }
        }}
      />
    </div>
  );
};

export default class OptionsAwareSidepanel extends React.Component {
  state = {
    isUsageAccepted: false,
    showVerificationResult: false,
    showExplanation: false,
    userId: '',
    clientSecret: undefined,
    keywordInstructions: undefined,
    sectionInstructions: undefined,
    quoteInstructions: undefined,
    modelVersion: undefined,
    numberOfVerifications: 0,
    hideDonationBanner: false,
    surveyBannerShownAt: undefined,
  };

  componentDidMount(): void {
    chrome.storage.local.get(
      [
        'isUsageAccepted',
        'showVerificationResult',
        'showExplanation',
        'userId',
        'clientSecret',
        'keywordInstructions',
        'sectionInstructions',
        'quoteInstructions',
        'modelVersion',
        'numberOfVerifications',
        'hideDonationBanner',
        'surveyBannerShownAt',
      ],
      (result) => {
        // Default set showVerificationResult and showExplanation to true, if they are undefined
        // Store the result in the local storage
        if (result.showVerificationResult === undefined) {
          result.showVerificationResult = true;
          chrome.storage.local.set({ showVerificationResult: true });
        }
        if (result.showExplanation === undefined) {
          result.showExplanation = true;
          chrome.storage.local.set({ showExplanation: true });
        }
        if (result.userId === undefined) {
          const userId = uuidv4();
          result.userId = userId;
          chrome.storage.local.set({ userId });
        }

        this.setState({
          isUsageAccepted: result.isUsageAccepted || false,
          showVerificationResult: result.showVerificationResult,
          showExplanation: result.showExplanation,
          userId: result.userId,
          clientSecret: result.clientSecret,
          keywordInstructions: result.keywordInstructions,
          sectionInstructions: result.sectionInstructions,
          quoteInstructions: result.quoteInstructions,
          modelVersion: result.modelVersion,
          numberOfVerifications: result.numberOfVerifications || 0,
          hideDonationBanner: result.hideDonationBanner || false,
          surveyBannerShownAt: result.surveyBannerShownAt || undefined,
        });
      }
    );
  }

  updateNumberOfVerifications = () => {
    console.log(
      `Currently on ${this.state.numberOfVerifications} verifications`
    );
    chrome.storage.local.get('numberOfVerifications', (result) => {
      const numberOfVerifications = (result.numberOfVerifications || 0) + 1;
      chrome.storage.local.set({ numberOfVerifications });
      this.setState({ numberOfVerifications });
      console.log(`Now on ${numberOfVerifications} verifications`);
    });
  };

  render() {
    const showSurveyBanner = this.state.surveyBannerShownAt === undefined;
    const showDonationBanner =
      !showSurveyBanner &&
      !this.state.hideDonationBanner &&
      this.state.numberOfVerifications -
        (this.state.surveyBannerShownAt || 0) >=
        NUM_VERIFICATIONS_BEFORE_DONATION_BANNER;
    return (
      <SidepanelWrappedWithState
        isUsageAccepted={this.state.isUsageAccepted}
        setIsUsageAccepted={(isUsageAccepted) => {
          this.setState({ isUsageAccepted });
          chrome.storage.local.set({ isUsageAccepted });
        }}
        showVerificationResult={this.state.showVerificationResult}
        showExplanation={this.state.showExplanation}
        userId={this.state.userId}
        keywordInstructions={this.state.keywordInstructions}
        sectionInstructions={this.state.sectionInstructions}
        quoteInstructions={this.state.quoteInstructions}
        clientSecret={this.state.clientSecret}
        modelVersion={this.state.modelVersion}
        showDonationBanner={showDonationBanner}
        showSurveyBanner={showSurveyBanner}
        hideDonationBanner={() => {
          chrome.storage.local.set({ hideDonationBanner: true });
          this.setState({ hideDonationBanner: true });
        }}
        hideSurveyBanner={() => {
          chrome.storage.local.set({
            surveyBannerShownAt: this.state.numberOfVerifications,
          });
          this.setState({
            surveyBannerShownAt: this.state.numberOfVerifications,
          });
        }}
        updateNumberOfVerifications={() => this.updateNumberOfVerifications()}
      />
    );
  }
}

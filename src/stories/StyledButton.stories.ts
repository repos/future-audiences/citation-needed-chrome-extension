import type { Meta, StoryObj } from '@storybook/react';
import { SelectedTextWrapped } from '../components/SelectedText';
import StyledButton from '../components/StyledButton';

const meta = {
  title: 'Sidepanel/StyledButton',
  component: StyledButton,
  parameters: {},
} satisfies Meta<typeof StyledButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    onClick: () => {},
    children: 'Continue reading on Wikipedia',
    primary: false,
  },
};

export const Primary: Story = {
  args: {
    ...Default.args,
    primary: true,
  },
};

export const Disabled: Story = {
  args: {
    ...Default.args,
    primary: false,
    disabled: true,
  },
};

export const PrimaryDisabled: Story = {
  args: {
    ...Default.args,
    primary: true,
    disabled: true,
  },
};

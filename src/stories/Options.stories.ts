import type { Meta, StoryObj } from '@storybook/react';
import Options from '../pages/Options/Options';

const meta = {
  title: 'Options/Options',
  component: Options,
  parameters: {},
} satisfies Meta<typeof Options>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {},
};

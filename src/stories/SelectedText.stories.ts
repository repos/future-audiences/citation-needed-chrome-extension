import type { Meta, StoryObj } from '@storybook/react';
import { SelectedTextWrapped } from '../components/SelectedText';

const meta = {
  title: 'Sidepanel/SelectedText',
  component: SelectedTextWrapped,
  parameters: {},
} satisfies Meta<typeof SelectedTextWrapped>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    verificationResult: undefined,
    showVerificationResult: false,
  },
};

export const WithOptions: Story = {
  args: {
    ...Default.args,
    showOptions: true,
    options: [
      'Vitamin A helps form and maintain healthy teeth, skeletal and soft tissue, mucus membranes, and skin.',
      'It is also known as retinol because it produces the pigments in the retina of the eye.',
      'Vitamin A promotes good eyesight, especially in low light. ',
    ],
  },
};

export const WithSelection: Story = {
  args: {
    verificationResult: undefined,
    showVerificationResult: false,
    allowSelectionMinimization: true,
    selection: `Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists. The earth-rattling event -- dubbed "Swift Quake" -- was not classified as an actual earthquake, but did record a maximum ground acceleration of about 0.011 meters per second squared, Western Washington University geology professor Jackie Caplan-Auerbach told The New York Times. The occurrence is being compared to 2011's "Beast Quake" in the same stadium, which was triggered by the NFL's Seattle Seahawks fans after a touchdown by Marshawn "Beast Mode" Lynch. `,
  },
};

export const WithTextArea: Story = {
  args: {
    verificationResult: undefined,
    showVerificationResult: false,
    showOptions: false,
    useTextArea: true,
    selection: `Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists. The earth-rattling event -- dubbed "Swift Quake" -- was not classified as an actual earthquake, but did record a maximum ground acceleration of about 0.011 meters per second squared, Western Washington University geology professor Jackie Caplan-Auerbach told The New York Times. The occurrence is being compared to 2011's "Beast Quake" in the same stadium, which was triggered by the NFL's Seattle Seahawks fans after a touchdown by Marshawn "Beast Mode" Lynch. `,
    allowSelectionMinimization: false,
  },
};

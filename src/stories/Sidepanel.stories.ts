import type { Meta, StoryObj } from '@storybook/react';
import { SidepanelWrapped } from '../pages/Sidepanel/Sidepanel';
import { VerificationResult } from '../helpers/verificationResults';
import { InteractionSteps } from '../helpers/constants';

const meta = {
  title: 'Sidepanel/Sidepanel',
  component: SidepanelWrapped,
  parameters: {},
} satisfies Meta<typeof SidepanelWrapped>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    isUsageAccepted: true,
    showVerificationResult: true,
    showExplanation: true,
    userId: '1234',
    setIsUsageAccepted: () => {},
    hideDonationBanner: () => {},
    updateNumberOfVerifications: () => {},
  },
};

export const DefaultWithWideSidepanel: Story = {
  args: {
    ...Default.args,
    style: { width: '500px' },
  },
};

export const WithShortSelection: Story = {
  args: {
    ...Default.args,
    selection: `Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists.`,
  },
};

export const WithLongSelection: Story = {
  args: {
    ...Default.args,
    useTextArea: true,
    selection: `Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists. The earth-rattling event -- dubbed "Swift Quake" -- was not classified as an actual earthquake, but did record a maximum ground acceleration of about 0.011 meters per second squared, Western Washington University geology professor Jackie Caplan-Auerbach told The New York Times. The occurrence is being compared to 2011's "Beast Quake" in the same stadium, which was triggered by the NFL's Seattle Seahawks fans after a touchdown by Marshawn "Beast Mode" Lynch. `,
  },
};

export const WithLongSelectionAndScrolling: Story = {
  args: {
    ...Default.args,
    useTextArea: true,
    selection: `Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists. The earth-rattling event -- dubbed "Swift Quake" -- was not classified as an actual earthquake, but did record a maximum ground acceleration of about 0.011 meters per second squared, Western Washington University geology professor Jackie Caplan-Auerbach told The New York Times. The occurrence is being compared to 2011's "Beast Quake" in the same stadium, which was triggered by the NFL's Seattle Seahawks fans after a touchdown by Marshawn "Beast Mode" Lynch. `,
    style: { height: '400px' },
  },
};

export const WithUsageNotAccepted: Story = {
  args: {
    ...Default.args,
    isUsageAccepted: false,
    useTextArea: true,
    selection: `Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists. The earth-rattling event -- dubbed "Swift Quake" -- was not classified as an actual earthquake, but did record a maximum ground acceleration of about 0.011 meters per second squared, Western Washington University geology professor Jackie Caplan-Auerbach told The New York Times. The occurrence is being compared to 2011's "Beast Quake" in the same stadium, which was triggered by the NFL's Seattle Seahawks fans after a touchdown by Marshawn "Beast Mode" Lynch. `,
  },
};

export const WithOptions: Story = {
  args: {
    ...Default.args,
    options: [
      'Vitamin A helps form and maintain healthy teeth, skeletal and soft tissue, mucus membranes, and skin.',
      'It is also known as retinol because it produces the pigments in the retina of the eye.',
      'Vitamin A promotes good eyesight, especially in low light. ',
    ],
  },
};

export const Verifiying: Story = {
  args: {
    ...Default.args,
    selection: `Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists.`,
    isVerifying: true,
  },
};

export const VerifiyingWithScrolling: Story = {
  args: {
    ...Default.args,
    selection: `Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists.`,
    isVerifying: true,
    style: { height: '400px' },
  },
};

export const VerifiyingWithCurrentStep: Story = {
  args: {
    ...Default.args,
    selection: `Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists.`,
    isVerifying: true,
    previousSteps: InteractionSteps.slice(0, 2),
    currentStep: InteractionSteps[2],
  },
};

export const WithResult: Story = {
  args: {
    isUsageAccepted: true,
    showVerificationResult: false,
    showExplanation: false,
    userId: '1234',
    verificationResult: VerificationResult.QUOTE_FOUND,
    selection: `Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists.`,
    page: {
      title: 'Impact of the Eras Tour',
      section: 'Seismic activity',
      snippet: `During the tour's stop in Seattle at Lumen Field on July 22 and 23, 2023, fans in the area caused seismic activity equivalent to a 2.3-magnitude eqrthquake, nichnamed the 'Swift Quake'. It was mostly attributed to the concert attendees jumping, dancing and cheering, as well as the loud sound system.`,
      page_url: 'https://en.wikipedia.org/wiki/Impact_of_the_Eras_Tour',
      section_url:
        'https://en.wikipedia.org/wiki/Impact_of_the_Eras_Tour#Seismic_activity',
      last_updated: '30 January 2024',
      references: 339,
      contributors: 83,
    },
    setIsUsageAccepted: () => {},
    hideDonationBanner: () => {},
    updateNumberOfVerifications: () => {},
  },
};

const argsWithResult = {
  isUsageAccepted: true,
  showVerificationResult: true,
  showExplanation: true,
  userId: '1234',
  selection: `Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists.`,
  explanation: 'According to our knowledge, this actually happened',
  page: {
    title: 'Impact of the Eras Tour',
    section: 'Seismic activity',
    snippet: `During the tour's stop in Seattle at Lumen Field on July 22 and 23, 2023, fans in the area caused seismic activity equivalent to a 2.3-magnitude eqrthquake, nichnamed the 'Swift Quake'. It was mostly attributed to the concert attendees jumping, dancing and cheering, as well as the loud sound system.`,
    page_url: 'https://en.wikipedia.org/wiki/Impact_of_the_Eras_Tour',
    section_url:
      'https://en.wikipedia.org/wiki/Impact_of_the_Eras_Tour#Seismic_activity',
    last_updated: '30 January 2024',
    references: 339,
    contributors: 83,
  },
  setIsUsageAccepted: () => {},
};

export const WithCorrectResult: Story = {
  args: {
    ...argsWithResult,
    verificationResult: VerificationResult.CORRECT,
  },
};

export const WithPartiallyCorrectResult: Story = {
  args: {
    ...argsWithResult,
    verificationResult: VerificationResult.PARTIALLY_CORRECT,
  },
};

export const WithIncorrectResult: Story = {
  args: {
    ...argsWithResult,
    verificationResult: VerificationResult.INCORRECT,
  },
};

export const WithNoResult: Story = {
  args: {
    isUsageAccepted: true,
    showVerificationResult: false,
    showExplanation: false,
    userId: '1234',
    verificationResult: VerificationResult.NO_RESULT,
    selection: `Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists.`,
    page: {
      title: 'Impact of the Eras Tour',
      snippet: `During the tour's stop in Seattle at Lumen Field on July 22 and 23, 2023, fans in the area caused seismic activity equivalent to a 2.3-magnitude eqrthquake, nichnamed the 'Swift Quake'. It was mostly attributed to the concert attendees jumping, dancing and cheering, as well as the loud sound system.`,
      page_url: 'https://en.wikipedia.org/wiki/Impact_of_the_Eras_Tour',
      section_url: 'https://en.wikipedia.org/wiki/Impact_of_the_Eras_Tour',
      last_updated: '30 January 2024',
      references: 339,
      contributors: 83,
    },
    setIsUsageAccepted: () => {},
    hideDonationBanner: () => {},
    updateNumberOfVerifications: () => {},
  },
};

export const WithNoArticles: Story = {
  args: {
    isUsageAccepted: true,
    showVerificationResult: false,
    showExplanation: false,
    userId: '1234',
    verificationResult: VerificationResult.NO_ARTICLES,
    selection: `Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists.`,
    setIsUsageAccepted: () => {},
    hideDonationBanner: () => {},
    updateNumberOfVerifications: () => {},
  },
};

export const WithError: Story = {
  args: {
    isUsageAccepted: true,
    showVerificationResult: false,
    showExplanation: false,
    userId: '1234',
    verificationResult: VerificationResult.ERROR,
    selection: `Taylor Swift is shaking it off the charts. One of the pop superstar's recent The Eras Tour performances at Lumen Field in Seattle, Washington, triggered seismic activity equivalent to a 2.3 magnitude earthquake, according to local seismologists.`,
    explanation: 'Something went wrong. Please try again later',
    setIsUsageAccepted: () => {},
    hideDonationBanner: () => {},
    updateNumberOfVerifications: () => {},
    hasErrored: true,
  },
};

export const WithMatchingUrl: Story = {
  args: {
    ...argsWithResult,
    verificationResult: VerificationResult.CORRECT,
    page: {
      ...argsWithResult.page,
      page_url:
        'https://en.wikipedia.org/wiki/Clear_Channel_memorandum?wprov=cna1',
    },
    currentUrl: 'https://en.wikipedia.org/wiki/Clear_Channel_memorandum',
  },
};

export const ShowingDonationBanner: Story = {
  args: {
    ...argsWithResult,
    verificationResult: VerificationResult.CORRECT,
    showDonationBanner: true,
  },
};

export const ShowingSurveyBanner: Story = {
  args: {
    ...argsWithResult,
    verificationResult: VerificationResult.CORRECT,
    showDonationBanner: false,
    showSurveyBanner: true,
  },
};

export const WithQuestionmarkDetails: Story = {
  args: {
    ...argsWithResult,
    verificationResult: VerificationResult.CORRECT,
    page: {
      ...argsWithResult.page,
      last_updated: '?',
      references: '?',
      contributors: '?',
    },
  },
};

export const WithOverflowingContent: Story = {
  args: {
    ...argsWithResult,
    verificationResult: VerificationResult.CORRECT,
    style: { height: '600px', width: '380px' },
  },
};

export const WithWideSidepanel: Story = {
  args: {
    ...argsWithResult,
    verificationResult: VerificationResult.CORRECT,
    style: { width: '500px' },
  },
};

export const OnNonSupportedSite: Story = {
  args: {
    ...Default.args,
    currentUrl: 'chrome://settings',
    isBadUrl: true,
  },
};

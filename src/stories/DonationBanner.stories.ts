import type { Meta, StoryObj } from '@storybook/react';
import { DonationBanner } from '../components/DonationBanner';

const meta = {
  title: 'Sidepanel/DonationBanner',
  component: DonationBanner,
  parameters: {},
} satisfies Meta<typeof DonationBanner>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {},
};

import type { Meta, StoryObj } from '@storybook/react';
import { SidepanelHeaderWrapped } from '../components/SidepanelHeader';

const meta = {
  title: 'Sidepanel/SidepanelHeader',
  component: SidepanelHeaderWrapped,
  parameters: {},
} satisfies Meta<typeof SidepanelHeaderWrapped>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {};

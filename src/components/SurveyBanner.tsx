import React from 'react';
import styles from './SurveyBanner.module.css';
import Icons from './icons';
import CatLaptop from '../assets/img/CatLaptop_2.gif';

export interface SurveyBannerProps {
  hideSurveyBanner: () => void;
}

export const SurveyBanner = (props: SurveyBannerProps) => {
  return (
    <div className={styles.container}>
      <div className={styles.headerRow}>
        <div className={styles.imageContainer}>
          <img src={CatLaptop} className={styles.cat} />
        </div>
        <div className={styles.closeButton}>
          <Icons.Close onClick={props.hideSurveyBanner} />
        </div>
      </div>
      <div className={styles.header}>Tell us more about your experience</div>
      <div className={styles.mainText}>
        Can you spare a few minutes? Take our quick survey to provide anonymous
        feedback and help us understand your experience with the Citation Needed
        browser extension.
      </div>
      <div className={styles.buttonRow}>
        <button
          className={styles.surveyButton}
          onClick={() => {
            window.open(
              'https://docs.google.com/forms/d/e/1FAIpQLSd4zZAW05yaX-5ioXrm2MYUOMDjEjee5UVoigrxLeCy-ZAaIg/viewform?usp=sf_link',
              '_blank'
            );
            props.hideSurveyBanner();
          }}
        >
          Take the survey
        </button>
        <button
          className={styles.alreadyButton}
          onClick={props.hideSurveyBanner}
        >
          Skip
        </button>
      </div>
    </div>
  );
};

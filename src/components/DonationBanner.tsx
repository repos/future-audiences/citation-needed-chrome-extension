import React from 'react';
import styles from './DonationBanner.module.css';
import Icons from './icons';
import WorldGif from '../assets/img/world.gif';
import { DONATION_URL } from '../helpers/constants';

export interface DonationBannerProps {
  hideDonationBanner: () => void;
}

export const DonationBanner = (props: DonationBannerProps) => {
  return (
    <div className={styles.container}>
      <div className={styles.headerRow}>
        <div className={styles.imageContainer}>
          <img src={WorldGif} className={styles.worldGif} />
        </div>
        <div className={styles.closeButton}>
          <Icons.Close onClick={props.hideDonationBanner} />
        </div>
      </div>
      <div className={styles.header}>Support the tech behind Wikipedia</div>
      <div className={styles.mainText}>
        From your searches to emerging AI technologies like Citation Needed,
        Wikipedia is the heart of online information.{' '}
        <b>Your support today strengthens the knowledge of tomorrow.</b>
      </div>
      <div className={styles.buttonRow}>
        <button
          className={styles.donateButton}
          onClick={() => {
            window.open(DONATION_URL, '_blank');
            props.hideDonationBanner();
          }}
        >
          Donate now
        </button>
        <button
          className={styles.alreadyDonatedButton}
          onClick={props.hideDonationBanner}
        >
          I've already donated
        </button>
      </div>
    </div>
  );
};

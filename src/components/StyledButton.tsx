import React, { ReactNode } from 'react';
import styles from './StyledButton.module.css';

const StyledButton = (props: {
  onClick: () => void;
  children: ReactNode;
  disabled?: boolean;
  primary?: boolean;
  width?: number;
}) => (
  <button
    className={[
      styles.button,
      props.disabled ? styles.disabled : '',
      props.primary ? styles.primary : styles.secondary,
    ].join(' ')}
    onClick={props.onClick}
    disabled={!!props.disabled}
    style={props.width ? { flexBasis: props.width } : {}}
  >
    {props.children}
  </button>
);

export default StyledButton;

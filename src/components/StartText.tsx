import React from 'react';
import styles from './StartText.module.css';
import HighlightGraphic from './icons/HighlightGraphic';

const StartText = () => (
  <div className={styles.container}>
    Select any statement to start
    <HighlightGraphic />
  </div>
);

export default StartText;

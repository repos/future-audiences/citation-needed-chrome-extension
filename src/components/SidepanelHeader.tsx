import React from 'react';
import Icons from './icons';
import styles from './SidepanelHeader.module.css';
import { VerificationResult } from '../helpers/verificationResults';

export interface PopupHeaderProps {
  isVerifying?: boolean;
  verificationResult?: VerificationResult;
}

const SidepanelHeader = (props: PopupHeaderProps) => (
  <div
    className={[
      styles.container,
      styles[props.isVerifying ? 'verifying' : props.verificationResult || ''],
    ].join(' ')}
  >
    <Icons.LabFlask className={styles.labFlask} />
    <h1 className={styles.headerTitle}>Generative AI experiment</h1>
    {
      // Disabling the info icon for the internal testing
      // <Icons.Info className={styles.infoIcon} />
    }
  </div>
);

export default SidepanelHeader;

export const SidepanelHeaderWrapped = () => (
  <div className={styles.wrapper}>
    <SidepanelHeader />
  </div>
);

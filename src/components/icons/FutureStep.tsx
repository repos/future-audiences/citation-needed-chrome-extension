import React from 'react';

const FutureStep = (props: { className?: string }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    stroke="#000"
    viewBox="0 0 24 24"
  >
    <g>
      <circle
        cx="12"
        cy="12"
        r="9.5"
        fill="none"
        stroke-width="2"
        stroke="#C8CCD1"
      />
    </g>
  </svg>
);

export default FutureStep;

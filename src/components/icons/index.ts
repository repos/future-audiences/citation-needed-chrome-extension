import Alert from './Alert';
import Bookmark from './Bookmark';
import Checkmark from './Checkmark';
import Clock from './Clock';
import FutureStep from './FutureStep';
import Info from './Info';
import Quotes from './Quotes';
import Spinner from './Spinner';
import UserAvatar from './UserAvatar';
import ErrorIcon from './ErrorIcon';
import Success from './Success';
import Logo from './Logo';
import InfoFilled from './InfoFilled';
import LabFlask from './LabFlask';
import Close from './Close';

const icons = {
  Bookmark,
  Checkmark,
  Clock,
  Close,
  FutureStep,
  Info,
  InfoFilled,
  Quotes,
  Spinner,
  UserAvatar,
  Alert,
  Error: ErrorIcon,
  Success,
  Logo,
  LabFlask,
};

export default icons;

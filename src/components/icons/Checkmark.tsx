import React from 'react';

const Checkmark = (props: { className?: string }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    stroke="#000"
    viewBox="0 0 24 24"
  >
    <g>
      <circle
        cx="12"
        cy="12"
        r="9.5"
        fill="none"
        stroke-width="2"
        stroke="#C8CCD1"
      />
    </g>
    <g>
      <circle
        cx="12"
        cy="12"
        r="9.5"
        fill="none"
        stroke-width="2"
        stroke="#36c"
      />
    </g>
    <path
      className="check"
      fill="#36C"
      stroke="none"
      d="M9.673.26a1 1 0 0 1 .067 1.413l-5 5.5a1 1 0 0 1-1.38.095l-3-2.5a1 1 0 0 1 1.28-1.536l2.264 1.887L8.26.327A1 1 0 0 1 9.673.26Z"
      transform="translate(7, 8.5)"
    />
  </svg>
);

export default Checkmark;

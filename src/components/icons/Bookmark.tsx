import React from 'react';

const Bookmark = (props: { className?: string }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="20"
    viewBox="0 0 20 20"
    {...props}
  >
    <title>bookmark</title>
    <path d="M5 1a2 2 0 0 0-2 2v16l7-5 7 5V3a2 2 0 0 0-2-2z" />
  </svg>
);

export default Bookmark;

import React from 'react';

const HighlightGraphic = (props: { className?: string }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="249"
    height="104"
    viewBox="0 0 249 104"
  >
    <path fill="#DFDFDF" d="M0 84h116v20H0V84Zm0-28h228v20H0z" />
    <path
      fill="#fff"
      fillRule="evenodd"
      d="M168.541 78.507a3 3 0 0 1 3.452-2.466l6 1a3 3 0 0 1-.986 5.918l-6-1a3 3 0 0 1-2.466-3.452Z"
      clipRule="evenodd"
    />
    <path
      fill="#202122"
      fillRule="evenodd"
      d="M170.514 78.835a1 1 0 0 1 1.15-.822l6 1a1 1 0 0 1-.328 1.973l-6-1a1 1 0 0 1-.822-1.15Z"
      clipRule="evenodd"
    />
    <path
      fill="#fff"
      fillRule="evenodd"
      d="M162.728 85.012a3 3 0 0 1 3.26 2.716l.5 5.5a3 3 0 1 1-5.976.544l-.5-5.5a3 3 0 0 1 2.716-3.26Z"
      clipRule="evenodd"
    />
    <path
      fill="#202122"
      fillRule="evenodd"
      d="M162.909 87.004a1 1 0 0 1 1.087.905l.5 5.5a1 1 0 0 1-1.992.182l-.5-5.5a1 1 0 0 1 .905-1.087Z"
      clipRule="evenodd"
    />
    <path
      fill="#fff"
      fillRule="evenodd"
      d="M168.879 84.879a3 3 0 0 1 4.242 0l2.5 2.5a3 3 0 1 1-4.242 4.242l-2.5-2.5a3 3 0 0 1 0-4.242Z"
      clipRule="evenodd"
    />
    <path
      fill="#202122"
      fillRule="evenodd"
      d="M170.293 86.293a1 1 0 0 1 1.414 0l2.5 2.5a1 1 0 1 1-1.414 1.414l-2.5-2.5a1 1 0 0 1 0-1.414Z"
      clipRule="evenodd"
    />
    <path fill="#447FF5" d="M30 56h127v20H30z" />
    <path fill="#DFDFDF" d="M0 28h208v20H0V28ZM0 0h249v20H0V0Z" />
    <mask
      id="a"
      width="20"
      height="38"
      x="146.5"
      y="45"
      fill="#000"
      maskUnits="userSpaceOnUse"
    >
      <path fill="#fff" d="M146.5 45h20v38h-20z" />
      <path
        fillRule="evenodd"
        d="M150.5 51h-2v-4h2a7.981 7.981 0 0 1 6 2.708 7.981 7.981 0 0 1 6-2.708h2v4h-2a4 4 0 0 0-4 4v7h3v4h-3v7a4 4 0 0 0 4 4h2v4h-2a7.981 7.981 0 0 1-6-2.708 7.981 7.981 0 0 1-6 2.708h-2v-4h2a4 4 0 0 0 4-4v-7h-3v-4h3v-7a4 4 0 0 0-4-4Z"
        clipRule="evenodd"
      />
    </mask>
    <path
      fill="#202122"
      fillRule="evenodd"
      d="M150.5 51h-2v-4h2a7.981 7.981 0 0 1 6 2.708 7.981 7.981 0 0 1 6-2.708h2v4h-2a4 4 0 0 0-4 4v7h3v4h-3v7a4 4 0 0 0 4 4h2v4h-2a7.981 7.981 0 0 1-6-2.708 7.981 7.981 0 0 1-6 2.708h-2v-4h2a4 4 0 0 0 4-4v-7h-3v-4h3v-7a4 4 0 0 0-4-4Z"
      clipRule="evenodd"
    />
    <path
      fill="#fff"
      d="M148.5 51h-2v2h2v-2Zm0-4v-2h-2v2h2Zm8 2.708-1.499 1.324 1.499 1.699 1.499-1.7-1.499-1.323Zm8-2.708h2v-2h-2v2Zm0 4v2h2v-2h-2Zm-6 11h-2v2h2v-2Zm3 0h2v-2h-2v2Zm0 4v2h2v-2h-2Zm-3 0v-2h-2v2h2Zm6 11h2v-2h-2v2Zm0 4v2h2v-2h-2Zm-8-2.708 1.499-1.324-1.499-1.699-1.499 1.7 1.499 1.323Zm-8 2.708h-2v2h2v-2Zm0-4v-2h-2v2h2Zm6-11h2v-2h-2v2Zm-3 0h-2v2h2v-2Zm0-4v-2h-2v2h2Zm3 0v2h2v-2h-2Zm-6-9h2v-4h-2v4Zm-2-6v4h4v-4h-4Zm4-2h-2v4h2v-4Zm7.499 3.385A9.979 9.979 0 0 0 150.5 45v4c1.792 0 3.398.783 4.501 2.032l2.998-2.647ZM162.5 45a9.979 9.979 0 0 0-7.499 3.385l2.998 2.647A5.983 5.983 0 0 1 162.5 49v-4Zm2 0h-2v4h2v-4Zm2 6v-4h-4v4h4Zm-4 2h2v-4h-2v4Zm-2 2a2 2 0 0 1 2-2v-4a6 6 0 0 0-6 6h4Zm0 7v-7h-4v7h4Zm-2 2h3v-4h-3v4Zm1-2v4h4v-4h-4Zm2 2h-3v4h3v-4Zm-1 9v-7h-4v7h4Zm2 2a2 2 0 0 1-2-2h-4a6 6 0 0 0 6 6v-4Zm2 0h-2v4h2v-4Zm2 6v-4h-4v4h4Zm-4 2h2v-4h-2v4Zm-7.499-3.385A9.98 9.98 0 0 0 162.5 83v-4a5.983 5.983 0 0 1-4.501-2.032l-2.998 2.647ZM150.5 83a9.98 9.98 0 0 0 7.499-3.385l-2.998-2.647A5.983 5.983 0 0 1 150.5 79v4Zm-2 0h2v-4h-2v4Zm-2-6v4h4v-4h-4Zm4-2h-2v4h2v-4Zm2-2a2 2 0 0 1-2 2v4a6 6 0 0 0 6-6h-4Zm0-7v7h4v-7h-4Zm2-2h-3v4h3v-4Zm-1 2v-4h-4v4h4Zm-2-2h3v-4h-3v4Zm1-9v7h4v-7h-4Zm-2-2a2 2 0 0 1 2 2h4a6 6 0 0 0-6-6v4Z"
      mask="url(#a)"
    />
  </svg>
);
export default HighlightGraphic;

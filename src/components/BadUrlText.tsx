import React from 'react';
import styles from './StartText.module.css';
import Error from './icons/ErrorIcon';

const BadUrlText = () => (
  <div className={styles.container}>
    Citation Needed can't run on this page.
  </div>
);

export default BadUrlText;

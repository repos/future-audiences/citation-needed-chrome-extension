import React, { useState } from 'react';
import styles from './SelectedText.module.css';
import { MAX_SELECTION_LENGTH } from '../helpers/selectionHelpers';
import { VerificationResult } from '../helpers/verificationResults';
import { VERIFICATION_RESULTS_TO_ALWAYS_SHOW } from '../helpers/constants';

const HIDDEN_SELECTION_WORD_BREAK = 70;
const HIDDEN_SELECTION_MAX_LENGTH = 80;
export interface WrappedSelectedTextProps {
  isVerifying?: boolean;
  options?: string[];
  selectedOptionIndex?: number;
  showOptions?: boolean;
  useTextArea?: boolean;
  selection?: string;
  showFullSelection?: boolean;
  allowSelectionMinimization?: boolean;
  verificationResult: VerificationResult | undefined;
  showVerificationResult: boolean;
}

export interface SelectedTextProps extends WrappedSelectedTextProps {
  setSelectedOptionIndex: (index: number) => void;
  setShowFullSelection?: (show: boolean) => void;
  setSelectedText?: (text: string) => void;
}

const SelectedText = (props: SelectedTextProps) => {
  const {
    isVerifying,
    options,
    selectedOptionIndex,
    showOptions,
    useTextArea,
    selection,
    showFullSelection,
    allowSelectionMinimization,
    verificationResult,
    showVerificationResult,
    setSelectedOptionIndex,
    setShowFullSelection,
  } = props;
  const verfificationResultClass =
    showVerificationResult ||
    VERIFICATION_RESULTS_TO_ALWAYS_SHOW.includes(
      verificationResult || VerificationResult.UNKNOWN
    )
      ? [styles[verificationResult || '']]
      : [];

  const containerClassNames = [
    styles.selectionText,
    ...[isVerifying ? [styles.isVerifying] : []],
    ...[verfificationResultClass],
  ].join(' ');

  if (useTextArea) {
    const isSelectionTooLong = (selection?.length || 0) > MAX_SELECTION_LENGTH;
    return (
      <div className={containerClassNames}>
        <div className={styles.selectionLabel}>
          Shorten the statement to continue
        </div>
        <textarea
          value={selection}
          className={
            isSelectionTooLong ? styles.textAreaTooLong : styles.textAreaOkay
          }
          onChange={(e) =>
            props.setSelectedText ? props.setSelectedText(e.target.value) : null
          }
        />
        <div
          className={
            isSelectionTooLong
              ? styles.characterCounterTooLong
              : styles.characterCounterOkay
          }
        >
          {selection?.length}/{MAX_SELECTION_LENGTH}
        </div>
      </div>
    );
  } else if (!options) {
    if ((selection?.length || 0) > HIDDEN_SELECTION_MAX_LENGTH) {
      if (!showFullSelection && allowSelectionMinimization) {
        // Trim selection to the next full word after 80 characters and add ellipsis
        // If there is no space after 80 characters, trim it to 100 characters
        let trimmedSelection = selection?.substring(
          0,
          HIDDEN_SELECTION_MAX_LENGTH
        );
        const relevantSpaceIndex = trimmedSelection?.indexOf(
          ' ',
          HIDDEN_SELECTION_WORD_BREAK
        );
        if (relevantSpaceIndex !== -1) {
          trimmedSelection = trimmedSelection?.substring(0, relevantSpaceIndex);
        }
        trimmedSelection = trimmedSelection?.concat(' …');

        return (
          <div className={containerClassNames}>
            {trimmedSelection}{' '}
            {setShowFullSelection && (
              <span
                className={styles.showMore}
                onClick={() => setShowFullSelection(true)}
              >
                Show more
              </span>
            )}
          </div>
        );
      } else if (allowSelectionMinimization) {
        return (
          <div className={containerClassNames}>
            {selection}{' '}
            {setShowFullSelection && (
              <span
                className={styles.showLess}
                onClick={() => setShowFullSelection(false)}
              >
                Show less
              </span>
            )}
          </div>
        );
      }
    }
    return <div className={containerClassNames}>{selection}</div>;
  }
  if (showOptions) {
    return (
      <div className={styles.selectionOptions}>
        {options.map((option, index) => (
          <div
            className={styles.selectionOption}
            key={option}
            onClick={() => {
              setSelectedOptionIndex(index);
            }}
          >
            <input type="radio" checked={index === selectedOptionIndex} />
            <div
              className={
                index === selectedOptionIndex
                  ? styles.selectionOptionSelected
                  : styles.selectionOptionUnselected
              }
            >
              {option}
            </div>
          </div>
        ))}
      </div>
    );
  }

  return (
    <div className={styles.selectionText}>
      {options[selectedOptionIndex || 0]}
    </div>
  );
};

export const SelectedTextWrapped = (props: WrappedSelectedTextProps) => {
  const [selectedOptionIndex, setSelectedOptionIndex] = useState<number>(0);
  const [showFullSelection, setShowFullSelection] = useState<boolean>(false);
  const [selectedText, setSelectedText] = useState<string>(
    props.selection || ''
  );
  return (
    <SelectedText
      {...props}
      selection={selectedText}
      selectedOptionIndex={selectedOptionIndex}
      showFullSelection={showFullSelection}
      setShowFullSelection={setShowFullSelection}
      setSelectedOptionIndex={setSelectedOptionIndex}
      setSelectedText={setSelectedText}
    />
  );
};

export default SelectedText;

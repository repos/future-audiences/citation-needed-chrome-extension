import React from 'react';
import styles from './WikipediaArticle.module.css';
import Icons from './icons';
import StyledButton from './StyledButton';

export interface WikipediaArticleProps {
  title: string;
  page_url: string;
  section_url: string;
  section?: string;
  snippet: string;
  last_updated: string;
  references: string | number;
  contributors: string | number;
  currentUrl?: string;
}

const WikipediaArticle = (props: WikipediaArticleProps) => {
  const {
    title,
    page_url,
    section_url,
    section,
    snippet,
    last_updated,
    references,
    contributors,
  } = props;

  const isOnMatchingWikiSite =
    props.currentUrl && page_url.startsWith(props.currentUrl);

  const hasMetadataResults =
    last_updated !== undefined &&
    references !== undefined &&
    contributors !== undefined &&
    last_updated !== '?' &&
    references !== '?' &&
    contributors !== '?';

  return (
    <div className={styles.container}>
      {isOnMatchingWikiSite ? (
        <div className={styles.currentlyOnWikiPage}>
          <div className={styles.linkHeaderLine}>
            <div className={styles.linkTitle}>{title}</div>
          </div>
        </div>
      ) : (
        <a
          href={page_url}
          target="_blank"
          rel="noreferrer"
          className={styles.wikiLink}
        >
          <div className={styles.linkHeaderLine}>
            <div className={styles.linkTitle}>{title}</div>
          </div>
        </a>
      )}
      <div className={styles.sectionBlock}>
        {section && <h2 className={styles.sectionTitle}>{section}</h2>}
        <div className={styles.sectionSnippet}>{snippet}</div>
      </div>
      {hasMetadataResults && (
        <div className={styles.footer}>
          <div className={styles.footerItem}>
            <Icons.Clock className={styles.footerItemIcon} />
            <span
              className={styles.footerItemText}
            >{`Latest edit ${last_updated}`}</span>
          </div>
          <div className={styles.footerItem}>
            <Icons.Bookmark className={styles.footerItemIcon} />

            <span
              className={styles.footerItemText}
            >{`${references} references`}</span>
          </div>
          <div className={styles.footerItem}>
            <Icons.UserAvatar className={styles.footerItemIcon} />
            <span
              className={styles.footerItemText}
            >{`${contributors} people worked on this article`}</span>
          </div>
        </div>
      )}

      {!isOnMatchingWikiSite && (
        <div className={styles.buttonContainer}>
          <StyledButton
            primary={true}
            onClick={() => {
              console.log(`Opening ${section_url}`);
              window.open(section_url, '_blank');
            }}
          >
            Continue reading on Wikipedia
          </StyledButton>
        </div>
      )}
    </div>
  );
};

export default WikipediaArticle;

import React from 'react';
import styles from './VerificationStatusBlock.module.css';
import { VerificationResult } from '../helpers/verificationResults';
import Icons from './icons';
import { VERIFICATION_RESULTS_TO_ALWAYS_SHOW } from '../helpers/constants';

export interface VerificationStatusBlockProps {
  result: VerificationResult;
  generatedExplanation: string | undefined;
  showExplanation: boolean;
  showVerificationResult: boolean;
}

const IconForStatus = (props: { result: VerificationResult }) => {
  switch (props.result) {
    case VerificationResult.CORRECT:
      return (
        <Icons.Success className={[styles.icon, styles.correct].join(' ')} />
      );
    case VerificationResult.INCORRECT:
      return (
        <Icons.Error className={[styles.icon, styles.incorrect].join(' ')} />
      );
    case VerificationResult.PARTIALLY_CORRECT:
      return (
        <Icons.Alert
          className={[styles.icon, styles.partially_correct].join(' ')}
        />
      );
    case VerificationResult.NO_RESULT:
      return (
        <Icons.InfoFilled
          className={[styles.icon, styles.no_result].join(' ')}
        />
      );
    case VerificationResult.NO_ARTICLES:
      return (
        <Icons.InfoFilled
          className={[styles.icon, styles.no_articles].join(' ')}
        />
      );
    case VerificationResult.ERROR:
      return (
        <Icons.InfoFilled className={[styles.icon, styles.error].join(' ')} />
      );
    default:
      return <></>;
  }
};

const textForStatus = {
  [VerificationResult.CORRECT]: 'This article backs it up.',
  [VerificationResult.INCORRECT]: 'This article goes against it.',
  [VerificationResult.PARTIALLY_CORRECT]:
    'This article that doesn’t quite settle it.',
  [VerificationResult.QUOTE_FOUND]:
    'Something similar is found in state in this article',
  [VerificationResult.NO_RESULT]: 'This article might be related.',
  [VerificationResult.NO_ARTICLES]: 'No relevant articles found.',
  [VerificationResult.UNKNOWN]: 'Unknown',
  [VerificationResult.ERROR]: 'Something went wrong. Please try again.',
};

const showExplationForStatus = {
  [VerificationResult.CORRECT]: true,
  [VerificationResult.INCORRECT]: true,
  [VerificationResult.PARTIALLY_CORRECT]: true,
  [VerificationResult.QUOTE_FOUND]: true,
  [VerificationResult.NO_RESULT]: false,
  [VerificationResult.NO_ARTICLES]: false,
  [VerificationResult.UNKNOWN]: false,
  [VerificationResult.ERROR]: false,
};

const VerificationStatusBlock = (props: VerificationStatusBlockProps) => (
  <div
    className={
      props.showVerificationResult ||
      VERIFICATION_RESULTS_TO_ALWAYS_SHOW.includes(props.result)
        ? [styles.container, styles[props.result]].join(' ')
        : styles.container
    }
  >
    {(props.showVerificationResult ||
      VERIFICATION_RESULTS_TO_ALWAYS_SHOW.includes(props.result)) && (
      <div className={[styles.status, styles[props.result]].join(' ')}>
        <IconForStatus result={props.result} />
      </div>
    )}
    {props.showExplanation && showExplationForStatus[props.result] ? (
      <div
        className={
          props.showVerificationResult ||
          VERIFICATION_RESULTS_TO_ALWAYS_SHOW.includes(props.result)
            ? [styles.explanation, styles[props.result]].join(' ')
            : styles.explanation
        }
      >
        {props.generatedExplanation}
      </div>
    ) : (
      <div
        className={
          props.showVerificationResult ||
          VERIFICATION_RESULTS_TO_ALWAYS_SHOW.includes(props.result)
            ? [styles.explanation, styles[props.result]].join(' ')
            : styles.explanation
        }
      >
        {textForStatus[props.result]}
      </div>
    )}
  </div>
);

export default VerificationStatusBlock;

import React from 'react';
import styles from './DotsAnimation.module.css';

const DotsAnimation = () => {
  const [dots, setDots] = React.useState('.');
  React.useEffect(() => {
    const interval = setInterval(() => {
      setDots((prev) => (prev.length < 3 ? prev + '.' : ''));
    }, 1000);
    return () => clearInterval(interval);
  }, []);
  return <span className={styles.container}>{dots}</span>;
};

export default DotsAnimation;

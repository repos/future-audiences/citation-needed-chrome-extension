import React from 'react';
import styles from './SidepanelFooter.module.css';
import { WikipediaArticleProps } from './WikipediaArticle';
import {
  FEEDBACK_FORM_EXPLANATION_FIELD_ID,
  FEEDBACK_FORM_QUOTE_FIELD_ID,
  FEEDBACK_FORM_REFERENCE_ID_FIELD_ID,
  FEEDBACK_FORM_SELECTION_FIELD_ID,
  FEEDBACK_FORM_URL,
} from '../helpers/constants';

export interface PopupFooterProps {
  selection?: string;
  article?: WikipediaArticleProps;
  explanation?: string;
  requestId?: string;
}

const makeUrlFriendly = (text: string) => {
  return (
    text
      // Replace plus
      .replace(/\+/g, '%2B')
      // Replace whitespace
      .replace(/ /g, '+')
      // Replace single quotes
      .replace(/'/g, '%27')
      // Replace double quotes
      .replace(/"/g, '%22')
      // Replace ampersand
      .replace(/&/g, '%26')
      // Replace hash
      .replace(/#/g, '%23')
      // Replace question mark
      .replace(/\?/g, '%3F')
      // Replace slash
      .replace(/\//g, '%2F')
      // Replace backslash
      .replace(/\\/g, '%5C')
      // Replace comma
      .replace(/,/g, '%2C')
      // Replace colon
      .replace(/:/g, '%3A')
      // Replace semicolon
      .replace(/;/g, '%3B')
      // Replace equals
      .replace(/=/g, '%3D')
  );
};

const SidepanelFooter = ({
  selection,
  article,
  explanation,
  requestId,
}: PopupFooterProps) => {
  const feedbackUrl =
    selection && article && explanation
      ? `${FEEDBACK_FORM_URL}` +
        `&${FEEDBACK_FORM_SELECTION_FIELD_ID}=${makeUrlFriendly(selection)}` +
        `&${FEEDBACK_FORM_QUOTE_FIELD_ID}=${makeUrlFriendly(
          `["${article.title}", "${article.section}", "${article.snippet}"]`
        )}` +
        `&${FEEDBACK_FORM_EXPLANATION_FIELD_ID}=${makeUrlFriendly(
          explanation
        )}` +
        `&${FEEDBACK_FORM_REFERENCE_ID_FIELD_ID}=${makeUrlFriendly(
          requestId || ''
        )}`
      : `${FEEDBACK_FORM_URL}`;

  return (
    <div
      className={[
        styles.container,
        ...[!selection ? [styles.noSelection] : []],
      ].join(' ')}
    >
      <a
        href="https://meta.wikimedia.org/wiki/Future_Audiences/Experiment:Citation_Needed"
        target="_blank"
      >
        About
      </a>{' '}
      ∙ <a onClick={() => chrome.runtime.openOptionsPage()}>Settings</a> ∙{' '}
      <a href={feedbackUrl} target="_blank">
        Report Issue
      </a>
    </div>
  );
};

export default SidepanelFooter;

export const SidepanelFooterWrapped = () => (
  <div className={styles.wrapper}>
    <SidepanelFooter />
  </div>
);

# Wikipedia Citation Needed Chrome Extension

- [Wikipedia Citation Needed Chrome Extension](#wikipedia-citation-needed-experiment-chrome-extension)
  - [Description](#description)
  - [Installing and Running](#installing-and-running)
    - [Procedures](#procedures)
    - [Running Storybook.js](#running-storybook.js)
    - [Building the extension](#building-the-extension)

## Description

This Chrome Extension is intended to be used with the Wikipedia Citation Needed API. It offers a User Interface to find a quote for a selected statement from Wikipedia using [ChatGPT](https://chat.openai.com) and the [Wikipedia Search API](https://www.mediawiki.org/wiki/API:Search).

## Installing and Running

### Procedures

1. Check if your [Node.js](https://nodejs.org/) version is >= **18**.
2. Run `npm install` to install the dependencies.
3. Run `npm start`
4. Load your extension on Chrome following:
   1. Access `chrome://extensions/`
   2. Check `Developer mode`
   3. Click on `Load unpacked extension`
   4. Select the `build` folder.

### Running Storybook.js

[Storybook.js](https://storybook.js.org/) is a framework for UI composition and testing.
This project uses Storybook.js for the development process.

To start the Storybook.js server, from the root directory, run `npm run storybook`

### Building the extension

From the root directory, run `npm run build`. The files will be generated into the `/build` directory, which can be loaded into chrome via `chrome://extensions` as described in Procedures.
